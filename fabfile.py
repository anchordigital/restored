import random

from fabric.api import cd, env, execute, local, parallel, roles, run, runs_once, task
from fabric.contrib.files import exists

# Changable settings
env.roledefs = {
    'staging': [
        'root@46.101.76.93',
    ],
    'production': [
        'root@178.128.38.52',
    ],
    'demo': [
        'restoredrelationships@trogdor.devsoc.org',
    ],
    'cron': [
        'restoredrelationships@smaug.devsoc.org',
    ],
}

#env.home = env.get('home', '/var/www/restoredrelationships')
env.home = env.get('home', 'restored')
env.repo = 'git@bitbucket.org:anchordigital/restored.git'
env.production_branch = 'master'
env.staging_branch = 'staging'


@task
def test():
    print("Inside the task!")


@task
def git_clone():
    run('git clone {} '.format(env.repo))

@task
@roles('staging')
def git_clone_staging():
    execute(git_clone)

@task
@roles('production')
def git_clone_production():
    execute(git_clone)

@task
@roles('staging')
@runs_once
def rebuild_staging():
    with cd(env.home):
        execute(rebuild, branch=env.staging_branch)

@task
@roles('production')
@runs_once
def rebuild_production():
    with cd(env.home):
        execute(rebuild, branch=env.production_branch)

@task
@runs_once
def rebuild(branch):
	with cd(env.home):
		# run("docker-compose up -d")
		execute(deploy, branch)
		run("docker-compose up --build -d")
		#run("docker-compose exec web python manage.py migrate")

@task
def migrate():
    with cd(env.home):
        run("docker-compose exec web python manage.py makemigrations")
        run("docker-compose exec web python manage.py migrate")

@task
@roles('staging')
def migrate_staging():
    with cd(env.home):
        # run("docker-compose up -d")
        execute(migrate)

@task
@roles('production')
def migrate_production():
    with cd(env.home):
        # run("docker-compose up -d")
        execute(migrate)

@task
@roles('staging')
def deploy_staging():
    with cd(env.home):
        # run("docker-compose up -d")
        execute(deploy, branch=env.staging_branch)
        #run("docker-compose up --build -d")
        #execute(migrate_staging)

@task
@roles('production')
def deploy_production():
    with cd(env.home):
        # run("docker-compose up -d")
        execute(deploy, branch=env.production_branch)
        #run("docker-compose up --build -d")
        #execute(migrate_production)

@task
def deploy(branch):
    with cd(env.home):
        # run("docker-compose up -d")
        run('git pull origin {} '.format(branch))
