import os

from .base import *  # noqa

DEBUG = True
TEMPLATES[0]["OPTIONS"]["debug"] = True

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": os.environ.get("POSTGRES_DB"),
        "USER": os.environ.get("POSTGRES_USER"),
        "PASSWORD": os.environ.get("POSTGRES_PASSWORD"),
        "PORT": "",
        'HOST': 'db'
    }
}

#        "NAME": os.environ.get("DJANGO_DATABASE_NAME", "restoredrelationships_django"),


INTERNAL_IPS = ["127.0.0.1", "0.0.0.0"]
import socket
hostname, _, ips = socket.gethostbyname_ex(socket.gethostname())
INTERNAL_IPS += [".".join(ip.split(".")[:-1] + ["1"]) for ip in ips]

ALLOWED_HOSTS = ["*"]
INSTALLED_APPS += ["django_extensions", "wagtail.contrib.styleguide"]
SECURE_REFERRER_POLICY = "no-referrer-when-downgrade"

# Webpack runserver
TEMPLATES[0]["OPTIONS"]["context_processors"].append("core.context_processors.browsersync")

# Use vanilla StaticFilesStorage to allow tests to run outside of tox easily
STATICFILES_STORAGE = "django.contrib.staticfiles.storage.StaticFilesStorage"

SECRET_KEY = "secret"

# EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

# Django debug toolbar - show locally unless DISABLE_TOOLBAR is enabled with environment vars
# eg. DISABLE_TOOLBAR=1 ./manage.py runserver
INSTALLED_APPS += ["debug_toolbar"]

MIDDLEWARE = ["debug_toolbar.middleware.DebugToolbarMiddleware"] + MIDDLEWARE

# Allow login with remote passwords, but downgrade/swap to crypt for password hashing speed
PASSWORD_HASHERS = [
    "django.contrib.auth.hashers.CryptPasswordHasher",
    "django.contrib.auth.hashers.PBKDF2PasswordHasher",
]

# Wagtail search - use Postgres unless ENABLE_ELASTICSEARCH is enabled with environment vars
# eg. ENABLE_ELASTICSEARCH=1 ./manage.py runserver
# if not os.environ.get("ENABLE_ELASTICSEARCH"):
#     INSTALLED_APPS += ["wagtail.contrib.postgres_search"]

#     WAGTAILSEARCH_BACKENDS = {"default": {"BACKEND": "wagtail.contrib.postgres_search.backend"}}
