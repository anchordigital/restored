"""
Django settings - Django 2.2.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.2/ref/settings/
"""

import os
import sys

import dj_database_url

import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

from dotenv import load_dotenv
load_dotenv()

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))

# Production / development switches
# https://docs.djangoproject.com/en/2.2/howto/deployment/checklist/

DEBUG = False

SECRET_KEY = os.environ.get("SECRET_KEY")

# Email
# https://docs.djangoproject.com/en/2.2/ref/settings/#email

ADMINS = [("Good Bear", "james@goodbear.co.uk")]
MANAGERS = ADMINS

MODERATORS = (("Moderators", "moderator@restoredrelationships.org"),)


SERVER_EMAIL = "info@restored-uk.org"
DEFAULT_FROM_EMAIL = "Restored <info@restored-uk.org>"
EMAIL_SUBJECT_PREFIX = "[restored] "

# # Mailgun
EMAIL_HOST = 'smtp.eu.mailgun.org'
EMAIL_PORT = 587
EMAIL_HOST_USER =  os.environ.get("EMAIL_HOST_USER")
EMAIL_HOST_PASSWORD = os.environ.get("EMAIL_HOST_PASSWORD")
EMAIL_USE_TLS = True
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

# ANYMAIL = {
#     # (exact settings here depend on your ESP...)
#     "MAILGUN_API_KEY": "f1f5f67c393d7b1c1094890208f025bc-9ad3eb61-7f929a79",
#     "MAILGUN_SENDER_DOMAIN": 'mg.restored-uk.org',  # your Mailgun domain, if needed
# }
# EMAIL_BACKEND = "anymail.backends.mailgun.EmailBackend"  # or sendgrid.EmailBackend, or...
# DEFAULT_FROM_EMAIL = "you@example.com"  # if you don't already have this in settings
# SERVER_EMAIL = "your-server@example.com"  # ditto (default from-email for Django errors)

# emails sent to clients who buy digital products from the store.
SALES_FROM_EMAIL = DEFAULT_FROM_EMAIL
SALES_EMAIL_PREFIX = EMAIL_SUBJECT_PREFIX

PROJECT_APPS_ROOT = os.path.join(BASE_DIR, "apps")
sys.path.append(PROJECT_APPS_ROOT)

DEFAULT_APPS = [
    # These apps should come first to load correctly.
    "core.apps.CoreConfig",
    "django.contrib.admin.apps.AdminConfig",
    "django.contrib.auth.apps.AuthConfig",
    "django.contrib.contenttypes.apps.ContentTypesConfig",
    "django.contrib.humanize",
    "django.contrib.sessions.apps.SessionsConfig",
    "django.contrib.messages.apps.MessagesConfig",
    "django.contrib.staticfiles.apps.StaticFilesConfig",
    "django.contrib.sitemaps.apps.SiteMapsConfig",
]

THIRD_PARTY_APPS = [
    "crispy_forms",
    "maskpostgresdata",
    "modelcluster",
    "paginationlinks",
    "rest_framework",
    "taggit",
    "wagtail.admin",
    "wagtail.contrib.forms",
    "wagtail.contrib.modeladmin",
    "wagtail.contrib.redirects",
    "wagtail.contrib.routable_page",
    "wagtail.contrib.search_promotions",
    "wagtail.contrib.settings",
    "wagtail.core",
    "wagtail.documents",
    "wagtail.embeds",
    "wagtail.images",
    "wagtail.search",
    "wagtail.sites",
    "wagtail.snippets",
    "wagtail.users",
    "wagtail_exportcsv",
    "wagtail_redirect_importer",
    "wagtailfontawesome",
    "cookie_consent",
    'captcha',
    'wagtailcaptcha',
    'wagtailgmaps',
    'wagtailmenus',
    'wagtailgeowidget'
]

PROJECT_APPS = [
    "accounts.apps.AccountsConfig",
    "categories.apps.CategoriesConfig",
    "mailinglists.apps.MailingListsConfig",
    "navigation.apps.NavigationConfig",
    "news.apps.NewsConfig",
    "contact_forms.apps.ContactFormsConfig",
    "pages",
    "redirect_pages.apps.RedirectPagesConfig",
    "resources.apps.ResourcesConfig",
    "settings.apps.SettingsConfig",
    "search.apps.SearchConfig",
    "beacons.apps.BeaconsConfig",
    "menu.apps.MenuConfig",
    # "locations.apps.LocationsConfig",
]

INSTALLED_APPS = DEFAULT_APPS + THIRD_PARTY_APPS + PROJECT_APPS

MIDDLEWARE = [
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "django.middleware.security.SecurityMiddleware",
    "wagtail.contrib.legacy.sitemiddleware.SiteMiddleware",
    "wagtail.contrib.redirects.middleware.RedirectMiddleware",
]

ROOT_URLCONF = "project.urls"

WSGI_APPLICATION = "project.wsgi.application"

AUTH_USER_MODEL = "accounts.User"

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DATABASES = {"default": dj_database_url.config()}

# Caches
# https://docs.djangoproject.com/en/2.2/topics/cache/

# CACHES = {}
# # if os.environ.get("REDIS_SERVERS"):
# #     CACHES["default"] = {
# #         "BACKEND": "django_redis.cache.RedisCache",
# #         "LOCATION": os.environ["REDIS_SERVERS"].split(" "),
# #         "KEY_PREFIX": "{}:cache".format(os.environ["REDIS_PREFIX"]),
# #     }
# # else:
#     CACHES["default"] = {"BACKEND": "django.core.cache.backends.locmem.LocMemCache"}

# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGE_CODE = "en-gb"

TIME_ZONE = "Europe/London"

USE_I18N = False

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/

STATIC_URL = os.environ.get("STATIC_URL", "/static/")

STATIC_ROOT = os.path.join(BASE_DIR, "htdocs/static")

STATICFILES_STORAGE = "django.contrib.staticfiles.storage.ManifestStaticFilesStorage"


STATICFILES_FINDERS = [
   'django.contrib.staticfiles.finders.FileSystemFinder',
   'django.contrib.staticfiles.finders.AppDirectoriesFinder',
]

STATICFILES_DIRS = [os.path.join(BASE_DIR, "static")]

# File uploads
# https://docs.djangoproject.com/en/2.2/ref/settings/#file-uploads

MEDIA_URL = os.environ.get("MEDIA_URL", "/media/")

MEDIA_ROOT = os.path.join(BASE_DIR, "htdocs/media")

# DEFAULT_FILE_STORAGE = os.environ.get(
#     "DEFAULT_FILE_STORAGE", "django.core.files.storage.FileSystemStorage"
# )

# Templates
# https://docs.djangoproject.com/en/2.2/ref/settings/#templates

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [os.path.join(BASE_DIR, "templates")],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.contrib.auth.context_processors.auth",
                "django.template.context_processors.i18n",
                "django.template.context_processors.media",
                "django.template.context_processors.static",
                "django.template.context_processors.tz",
                "django.template.context_processors.request",
                "django.contrib.messages.context_processors.messages",
                "core.context_processors.demo",
                "wagtail.contrib.settings.context_processors.settings",
                "categories.context_processors.categories",
                "resources.context_processors.resource_choices",
                "django.template.context_processors.request",
                'wagtailmenus.context_processors.wagtailmenus',
            ]
        },
    }
]

# Logging
# https://docs.djangoproject.com/en/2.2/topics/logging/#configuring-logging

# LOGGING = {
#     "version": 1,
#     "disable_existing_loggers": False,
#     "filters": {
#         "require_debug_false": {"()": "django.utils.log.RequireDebugFalse"},
#         "require_debug_true": {"()": "django.utils.log.RequireDebugTrue"},
#     },
#     "formatters": {
#         "django.server": {
#             "()": "django.utils.log.ServerFormatter",
#             "format": "[{server_time}] {message}",
#             "style": "{",
#         }
#     },
#     "handlers": {
#         "console": {
#             "level": "INFO",
#             "filters": ["require_debug_true"],
#             "class": "logging.StreamHandler",
#         },
#         "django.server": {
#             "level": "INFO",
#             "formatter": "django.server",
#             "class": "logging.StreamHandler",
#         },
#         "null": {"class": "logging.NullHandler"},
#         'applogfile': {
#             'level':'DEBUG',
#             'class':'logging.handlers.RotatingFileHandler',
#             'filename': os.path.join(BASE_DIR, 'APPNAME.log'),
#             'maxBytes': 1024*1024*15, # 15MB
#             'backupCount': 10,
#         },
#     },
#     "loggers": {
#         "django": {"handlers": ["console"], "level": "INFO"},
#         "django.server": {"handlers": ["django.server"], "level": "INFO", "propagate": False},
#         "py.warnings": {"handlers": ["console"]},
#         'PROJECT': {
#             'handlers': ['applogfile',],
#             'level': 'DEBUG',
#         },
#     },
# }

# Sites framework
SITE_ID = 1

# # Cloud storage
# CONTENTFILES_PREFIX = os.environ.get("CONTENTFILES_PREFIX", "restoredrelationships")
# CONTENTFILES_HOSTNAME = os.environ.get("CONTENTFILES_HOSTNAME")
# CONTENTFILES_S3_ENDPOINT = os.environ.get("CONTENTFILES_S3_ENDPOINT")

# Improved cookie security
CSRF_COOKIE_HTTPONLY = True

# Wagtail
WAGTAIL_SITE_NAME = "restoredrelationships"
BASE_URL = os.environ.get("WAGTAIL_BASE_URL", "")
WAGTAILADMIN_BASE_URL = os.environ.get("WAGTAIL_BASE_URL", "")
WAGTAIL_ENABLE_UPDATE_CHECK = False
WAGTAILDOCS_DOCUMENT_MODEL = "resources.File"
# WAGTAILSEARCH_BACKENDS = {
#     "default": {
#         "BACKEND": "wagtail.contrib.postgres_search.backend",
#         # "URLS": [os.environ.get("SEARCH_URL", "http://127.0.0.1:9200")],
#         # "INDEX": os.environ.get("SEARCH_INDEX", "restoredrelationships"),
#     }
# }
WAGTAIL_USER_EDIT_FORM = "accounts.forms.CustomUserEditForm"
WAGTAIL_USER_CREATION_FORM = "accounts.forms.CustomUserCreateForm"
WAGTAIL_USER_CUSTOM_FIELDS = ["location", "activation_key"]


# Django limits POST fields to 1,000 by default, however for Wagtail admin pages this is too low
DATA_UPLOAD_MAX_NUMBER_FIELDS = 10000

DEMO_SITE = False

# Accounts/login tweaks
LOGIN_REDIRECT_URL = "/"
SESSION_EXPIRE_AT_BROWSER_CLOSE = True

# Mailchimp
MAILCHIMP_USER_NAME = os.environ.get("MAILCHIMP_USER_NAME")
MAILCHIMP_KEY = os.environ.get("MAILCHIMP_KEY")
MAILCHIMP_LIST_ID = os.environ.get("MAILCHIMP_LIST_ID")
MAILCHIMP_GROUP_NAME = os.environ.get("MAILCHIMP_GROUP_NAME")

# Square online store
SQUARE_APPLICATION_ID = os.environ.get("SQUARE_APPLICATION_ID")
SQUARE_ACCESS_TOKEN = os.environ.get("SQUARE_ACCESS_TOKEN")
SQUARE_ENVIRONMENT = os.environ.get("SQUARE_ENVIRONMENT", "sandbox")

# Cookie Consent
COOKIE_CONSENT_NAME = "cookie_consent"
COOKIE_CONSENT_MAX_AGE = 60 * 60 * 24 * 365 * 1  # 1 year
COOKIE_CONSENT_LOG_ENABLED = True

# Recaptcha
RECAPTCHA_PUBLIC_KEY = str(os.environ.get("RECAPTCHA_PUBLIC_KEY"))
RECAPTCHA_PRIVATE_KEY = str(os.environ.get("RECAPTCHA_PRIVATE_KEY"))
NOCAPTCHA = True

# Wagtail Gmaps
# Mandatory
WAGTAIL_ADDRESS_MAP_CENTER = 'London, United Kingdom'  # It must be a properly formatted address
WAGTAIL_ADDRESS_MAP_KEY = 'AIzaSyDBXAeRRgYQYCwg_1J3sX69xukcFG13uE4'
GOOGLE_MAPS_V3_APIKEY = 'AIzaSyDBXAeRRgYQYCwg_1J3sX69xukcFG13uE4'
# Optional
WAGTAIL_ADDRESS_MAP_ZOOM = 8  # See https://developers.google.com/maps/documentation/javascript/tutorial#MapOptions for more information.
WAGTAIL_ADDRESS_MAP_LANGUAGE = 'gb'  # See https://developers.google.com/maps/faq#languagesupport for supported languages.

WAGTAILMENUS_MAIN_MENU_ITEMS_RELATED_NAME = "custom_menu_items"
# WAGTAILMENUS_MAIN_MENU_MODEL = "menu.CustomMainMenu"

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'
