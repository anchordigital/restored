document.addEventListener('scroll', function() {
    const navbar = document.querySelector('.breadcrumb-bar');
    const navbarHeight = 88;

    const distanceFromTop = Math.abs(document.body.getBoundingClientRect().top);

    if (distanceFromTop >= navbarHeight) {
        navbar.classList.add('fixed-top');
    } else {
        navbar.classList.remove('fixed-top');
    }
});
