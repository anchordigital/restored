/**
 * Handle expanding and collapsing in tree navigation
 *
 * This was based on the following code from the a11y style guide:
 * https://github.com/cehfisher/a11y-style-guide/blob/5cd049e798d3d2493287d15b991fb36693038e91/docs/js/accordion.js
 */

const toggleList = document.querySelectorAll('.tree-nav__toggle');

function toggleSubTree(toggle) {
    const subTree = toggle.nextElementSibling;
    const isExpanded = toggle.getAttribute('aria-expanded') === 'true';

    if (isExpanded) {
        subTree.setAttribute('aria-hidden', false);
        toggle.setAttribute('aria-expanded', false);
        toggle.parentElement.classList.add('is-open');
    } else {
        subTree.setAttribute('aria-hidden', true);
        toggle.setAttribute('aria-expanded', true);
        toggle.parentElement.classList.remove('is-open');
    }
}

toggleList.forEach((toggle) => {
    toggle.addEventListener('click', () => toggleSubTree(toggle));
});

const menu_toggle = document.getElementById('nav-toggle');
const body = document.querySelector('body');

menu_toggle.addEventListener('change', () => {
    if (menu_toggle.checked) {
        body.classList.add('no-scroll');
    } else {
        body.classList.remove('no-scroll');
    }
});

window.addEventListener('resize', () => {
    const vh = window.innerHeight * 0.01;
    // set the value in the --vh custom property to the root of the document
    document.documentElement.style.setProperty('--vh', `${vh}px`);
});

const menuItemList = document.querySelectorAll('.nav-item-top');
var bgOverlay = document.querySelector('.menu-bg-overlay');

// menuItemList.addEventListener("mouseover", () => {
//     alert('test');
// });

menuItemList.forEach((toggle) => {
    toggle.addEventListener('mouseenter',  () => {
        bgOverlay.classList.add('menu-bg-overlay--show');
    })
    toggle.addEventListener('mouseleave',  () => {
        bgOverlay.classList.remove('menu-bg-overlay--show');
    })
});
