var map;
var prev_infowindow;
var geocoder;
window.center_points = [];

function initMap(){
    map_div = document.getElementById('map');
    var zoom = map_div.getAttribute('data-zoom');
    console.log(zoom);
    map = new google.maps.Map(document.getElementById('map'), {
      zoom: parseInt(zoom),
      gestureHandling: 'greedy',
      scrollwheel: false,
      center: { lat: 51.5074, lng: 0.1278 } 
    });
    map.maxDefaultZoom = 15;
    var mapReadyEvent = new CustomEvent('map-ready', );
    window.dispatchEvent(mapReadyEvent);

    google.maps.event.addListenerOnce(map, "bounds_changed", function () {
      this.setZoom(Math.min(this.getZoom(), this.maxDefaultZoom));
    });

    // console.log('12');
}
initMap();
// function populateMap(){})
//   return {

//     markers: {},
//     // prev_infowindow:'',

    function initMarkers() {
        if(google != undefined){
          map.markers = [];
          var markers = document.getElementsByClassName("marker");
          let marker;
          geocoder = new google.maps.Geocoder();
          console.log(markers);
          for (var i = 0; i < markers.length; i++) {

            var id =  markers[i].getAttribute('data-id');
            var title =  markers[i].getAttribute('data-name');
            var addressText = markers[i].getAttribute('data-address');
            var latLng = {
                lat: parseFloat( markers[i].getAttribute('data-lat') ),
                lng: parseFloat( markers[i].getAttribute('data-lng') )
            };

            // console.log(latLng);

            this.addMarker(latLng, addressText, title, id);
            window.center_points.push(latLng);
            this.centerMap(center_points);

            // geocoder.geocode( { 'address': address}, function(results, status) {
            //   if (status == google.maps.GeocoderStatus.OK) {
            //     if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
            //       // console.log(results[0].geometry.location.lat);
            //       window.center_points.push(results[0].geometry.location);
            //       // markers[i].setAttribute('lat', results[0].geometry.location.lat);
            //       // markers[i].setAttribute('lng', results[0].geometry.location.lng);
            //       console.log(results[0].geometry.location);
            //       this.addMarker(results[0].geometry.location, title);

            //       this.centerMap(center_points);

            //     } else {
            //       alert("No results found");
            //     }
            //   } else {
            //     alert("Geocode was not successful for the following reason: " + status);
            //   }
            // });

          }
      }
    }

    function addMarker(address, addressText, title, id) {

      // var latLng = {
      //     lat: parseFloat( lat ),
      //     lng: parseFloat( lng )
      // };

      // Create marker instance.
      var marker = new google.maps.Marker({
          position : address,
          map: map,
          label: {
            text: id,
            color: "#fff",
            fontSize: "12px",
            fontWeight: "bold"
          },
      });

      // Append to reference for later use.
      map.markers.push( marker );

      // If marker contains HTML, add it to an infoWindow.

      // Create info window.
      var infowindow = new google.maps.InfoWindow({
          content: "<h5>"+title+"</h5><p>"+addressText+"</p><a class='button' href='#''>Find out more</a>"
      });

      // Show info window when marker is clicked.
      google.maps.event.addListener(marker, 'click', function() {
        console.log(prev_infowindow);
          if( prev_infowindow ) {
             prev_infowindow.close();
          }

          prev_infowindow = infowindow;
          console.log(prev_infowindow);
          infowindow.open( map, marker );
      });
      
    }

    function centerMap(center_points){
      var bounds = new google.maps.LatLngBounds();
      for (var i = 0; i < center_points.length; i++) {
        // console.log(window.center_points[i]);
        bounds.extend(window.center_points[i]);
      }
      map.fitBounds(bounds);
    }
  


initMap();
initMarkers();

const form = document.getElementById("address-form")
form.addEventListener("submit", (e) => {
  e.preventDefault();

  const formData = new FormData(form);
  address = formData.get("address");
  services = formData.get("services");
  if(address){
    // location = locationSearch(address);
    fetch('https://maps.googleapis.com/maps/api/geocode/json?address='+address+'&key=AIzaSyAHeprGQANUYkylWrrZBY-lTBk8CaS7I-Y')
      .then(res => res.json())
      .then(data => {
        lat = data.results[0].geometry.location.lat;
        lng = data.results[0].geometry.location.lng;
        // return{
        //     lat:lat,
        //     lng:lng
        // }
        if(services){
          window.location.replace('/beacons/search?lat='+lat+'&lng='+lng+'&services='+services);
        } else {
          window.location.replace('/beacons/search?lat='+lat+'&lng='+lng);
        }
      });
    } else {
      if(services){
        window.location.replace('/beacons/search?services='+services);
      } else {
        window.location.replace('/beacons/search?lat='+lat+'&lng='+lng);
      }
    }



});

// function locationSearch(address) {
  
//   address = address + " UK";
//   // this.isLoading = true;
//   fetch('https://maps.googleapis.com/maps/api/geocode/json?address='+address+'&key=AIzaSyAHeprGQANUYkylWrrZBY-lTBk8CaS7I-Y')
//     .then(res => res.json())
//     .then(data => {
//       lat = data.results[0].geometry.location.lat;
//       lng = data.results[0].geometry.location.lng;
//       // return{
//       //     lat:lat,
//       //     lng:lng
//       // }
//       // window.location.replace('/beacons/search?lat='+lat+'&lng='+lng);
//     });
// }

function userLocationSearch(){
  finding_location = true;

  services = $( "#beacon-services" ).val();

  navigator.geolocation.getCurrentPosition(
    (position) => {
      const pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude,
      };

      console.log(pos);
      if( services != ''){
        window.location.replace('/beacons/search?lat='+lat+'&lng='+lng+'&services='+services);
      } else {
        window.location.replace('/beacons/search?lat='+lat+'&lng='+lng);
      }
    },
    () => {
      // handleLocationError(true, infoWindow, map.getCenter());
     alert("Your browser doesn't support Geolocation");
    }
  );
}

