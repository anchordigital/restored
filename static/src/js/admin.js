/* Each additional feature / change to the admin kept in it's own object here.
 * As things get added dynamically, all the events are body / global level.
 *
 * (Also from dev churchinwales project)
 * */

const showOnlyValidLinkOptions = {
    // For Links, only show the selected field types.  So if 'Page Link' is selected,
    // only show the Page Chooser.  If URL field, only show the URL field. Etc.

    setRelatedFieldsVisibility(link_type_selector) {
        const value = link_type_selector.value;
        const parent = link_type_selector.closest('.link_block'),
            page_link = parent.querySelector('.page_link_field'),
            file_link = parent.querySelector('.file_link_field'),
            custom_url_link = parent.querySelector('.custom_url_link_field'),
            new_window_toggle = parent.querySelector('.new_window_link_field');

        // this is repetative, but I don't mind.  No magic.
        if (value === 'page') {
            page_link.classList.remove('hidden');
            file_link.classList.add('hidden');
            custom_url_link.classList.add('hidden');
            new_window_toggle.classList.remove('hidden');
        } else if (value === 'file') {
            page_link.classList.add('hidden');
            file_link.classList.remove('hidden');
            custom_url_link.classList.add('hidden');
            new_window_toggle.classList.add('hidden');
        } else if (value === 'custom_url') {
            page_link.classList.add('hidden');
            file_link.classList.add('hidden');
            custom_url_link.classList.remove('hidden');
            new_window_toggle.classList.remove('hidden');
        } else {
            page_link.classList.add('hidden');
            file_link.classList.add('hidden');
            custom_url_link.classList.add('hidden');
            new_window_toggle.classList.add('hidden');
        }
    },

    onload() {
        const active_url_selectors = [
            ...document.querySelectorAll('.link_choice_type_selector select')
        ];

        // Show link options if a link has been chosen
        active_url_selectors.forEach(this.setRelatedFieldsVisibility);
    },

    onchange(event) {
        const target = event.target,
            link_choice_div = target.closest('.link_choice_type_selector');

        if (link_choice_div !== null) {
            this.setRelatedFieldsVisibility(target);
        }
    }
};

const twoColumnBlock = {
    // Two Column Blocks should actually display as two columns if the screen is wide enough.

    setRelatedTwoColumnWidths(target) {
        const parent = target.closest('.two_column_block');

        parent.classList.remove(
            'two_column_halves',
            'two_column_left',
            'two_column_right',
            'two_column_left-extra',
            'two_column_right-extra'
        );
        parent.classList.add(`two_column_${target.value}`);
    },

    onload() {
        const two_column_selectors = document.querySelectorAll('.two_column_block select');
        two_column_selectors.forEach((el) => {
            if (el.name.endsWith('-value-alignment')) {
                this.setRelatedTwoColumnWidths(el);
            }
        });
    },

    onchange(event) {
        if (event.target.name.endsWith('-value-alignment')) {
            this.setRelatedTwoColumnWidths(event.target);
        }
    }
};

/* And here are the global handlers that call the above feature objects */

document.body.onchange = (event) => {
    showOnlyValidLinkOptions.onchange(event);
    twoColumnBlock.onchange(event);
};

window.onload = () => {
    showOnlyValidLinkOptions.onload();
    twoColumnBlock.onload();
};
