// @codekit-prepend "transition.js";
// @codekit-prepend "carousel.js";
// @codekit-prepend "affix.js";
// @codekit-prepend "modal.js";

/* global $ */

$().ready(function() {
    // Video popup
    const video_modal = $('#video-modal');

    video_modal.on('show.bs.modal', function(event) {
        const target = $(event.relatedTarget);
        const video = $(target).data('video');
        $(this).append(
            `<iframe width="560" height="315" src="https://www.youtube.com/embed/${video}?autoplay=1" frameborder="0" allowfullscreen=""></iframe>`
        );
        $("[data-ride='carousel']").carousel('pause');
    });

    video_modal.on('hidden.bs.modal', function() {
        $(this).empty();
        $("[data-ride='carousel']").carousel('cycle');
    });

    $('#registration-form').on('submit', function() {
        const email = $('#id_email').val();
        const reversed_email = email
            .split('')
            .reverse()
            .join('');
        $('#id_anti_spam').val(reversed_email);
    });
});
