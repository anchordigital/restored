from . import choices


def resource_choices(request):
    # So in the templates we can do:
    # {% if resource.type == resource_choices.PUBLIC_DOWNLOAD %} etc...
    return {"resource_choices": choices}
