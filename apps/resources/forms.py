from django import forms
from django.core.exceptions import ValidationError
from django.db import transaction
from django.core import mail

from .models import ResourceDownloadRequest, ResourcePrintedCopyRequest


class DownloadResourceForm(forms.ModelForm):
    class Meta:
        model = ResourceDownloadRequest
        exclude = ("date",)
        widgets = {"file": forms.HiddenInput(), "resource_page": forms.HiddenInput()}

    # def save(self):
    #     instance = super(DownloadResourceForm, self).save()
    #     transaction.on_commit(send_resource_email)

    def clean(self):
        # A resource_page and file must be submitted, and the file must be listed as a
        # free_resource on that page.
        try:
            page = self.cleaned_data["resource_page"]

            if not page.free_resources.filter(file=self.cleaned_data["file"]):
                raise ValidationError("That resource is not available on that page")

        except (KeyError, ValueError):
            raise ValidationError("That resource is not available on that page")

    error_css_class = "error"
    required_css_class = "required"


class ResourcePrintedCopyRequestForm(forms.ModelForm):
    class Meta:
        model = ResourcePrintedCopyRequest
        exclude = ("resource", "resource_file", "date")

    error_css_class = "error"
    required_css_class = "required"

def send_resource_email():
    mail.send_mail(
        "Subject here",
        "Here is the message.",
        "from@example.com",
        ["james@goodbear.co.uk"],
        fail_silently=False,
    )
