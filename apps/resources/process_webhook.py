"""
    This module is for processing webhooks coming from Square, whenever an order is updated.
"""

import hmac
import json
from base64 import standard_b64encode
from uuid import uuid4

from django.conf import settings
from django.core import mail
from django.template.loader import get_template

from wagtail.core.models import Site

from . import choices
from .models import File, OrderProcessed, UniqueDownloadCode
from .square import SquareClient


class InvalidWebhookPayload(Exception):
    pass


class MissingData(InvalidWebhookPayload):
    pass


def validate_webhook_signature(request, signature_key):
    """
        When a webhook comes in from Square, it has a X-Square-Signature that we
        can check to see if it's actually from them.
    """
    # see https://developer.squareup.com/docs/webhooks-api/validate-notifications#step-2-validate-the-signature  # noqa

    # use hard-coded https over .scheme or better .get_raw_uri, to make ngrok work correctly,
    # and also, doing this with https is a BAD idea.
    uri = f"https://{request.get_host()}{request.get_full_path()}"
    combined = uri.encode() + request.body

    signature = standard_b64encode(hmac.digest(signature_key.encode(), combined, "sha1")).decode()

    return signature == request.headers.get("X-Square-Signature")


def get_webhook_data(request):
    """
        Attempt to load the data from the request, and check it's the right type of data
        for us to process.
    """
    try:
        payload = json.loads(request.body)
    except json.decoder.JSONDecodeError:
        raise InvalidWebhookPayload("Invalid JSON")

    if not payload.get("type") == "order.updated":
        raise InvalidWebhookPayload("Unknown Webhook Type")

    data = payload.get("data", {})

    if not data.get("type") == "order":
        raise InvalidWebhookPayload("Not an order")

    return data


def generate_unique_codes(skus, email_addresses, order):
    """
        Given SKUs sent back from the order lookup in square, find any files
        which match those SKUs, and generate some UniqueDownloadCodes for those files,
        with the email_addresses as reference.
    """
    unique_codes = []
    for file in File.objects.filter(sku__in=skus):
        unique_codes.append(
            UniqueDownloadCode.objects.create(
                file=file, reference=",".join(email_addresses), code=str(uuid4()), order=order
            )
        )
    return unique_codes


def send_email_with_unique_codes(email_addresses, unique_codes, url_prefix, webhook_settings):
    """
        Send an email to all the email_addresses containing links to all their unique downloads.
    """

    if unique_codes:
        email_template = get_template("resources/emails/send_unique_codes.txt")

        mail.send_mail(
            f"{settings.SALES_EMAIL_PREFIX} Thankyou for buying our resource!",
            email_template.render(
                {
                    "email_top": webhook_settings.email_top,
                    "email_foot": webhook_settings.email_foot,
                    "url_prefix": url_prefix,
                    "unique_codes": unique_codes,
                }
            ),
            settings.SALES_FROM_EMAIL,
            email_addresses,
        )


def process_webhook(request, webhook_settings):
    """
        The view validates that this is a valid web-hook (via the signature),
        so now we just need to try and process it.
    """
    url_prefix = f"https://{Site.find_for_request(request).hostname}"

    # Get data from the request:
    data = get_webhook_data(request)

    # Pull out the fields we need:
    order_id = data.get("id")
    order_state = data.get("object", {}).get("order_updated", {}).get("state")
    location_id = data.get("object", {}).get("order_updated", {}).get("location_id")

    # Confirm they're all OK:
    if not all((order_id, order_state, location_id)):
        raise MissingData("Required information missing")

    order, created = OrderProcessed.objects.get_or_create(order_id=order_id)

    if order_state == choices.ORDER_CANCELLED:
        order.state = choices.ORDER_CANCELLED
        order.uniquedownloadcode_set.all().delete()
        order.save()
        return
    else:
        if not created and order.state == choices.ORDER_COMPLETED:
            return

    # Connect to Square and get all the extra information we need that they
    # don't put into the web-hook payload:
    client = SquareClient()

    matching_orders = client.get_orders(order_id, location_id)
    skus = client.get_skus_from_orders(matching_orders)
    email_addresses = client.get_email_address_from_orders(matching_orders)

    # All good so far, so generate some unique codes and sent them to the customer:
    unique_codes = generate_unique_codes(skus, email_addresses, order)
    send_email_with_unique_codes(email_addresses, unique_codes, url_prefix, webhook_settings)

    order.state = choices.ORDER_COMPLETED
    order.save()
