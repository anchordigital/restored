import os

from square.client import Client

# Square online store
SQUARE_APPLICATION_ID = os.environ.get("SQUARE_APPLICATION_ID")
SQUARE_ACCESS_TOKEN = os.environ.get("SQUARE_ACCESS_TOKEN")
SQUARE_ENVIRONMENT = os.environ.get("SQUARE_ENVIRONMENT", "sandbox")


client = Client(access_token=SQUARE_ACCESS_TOKEN, environment=SQUARE_ENVIRONMENT)

print("getting locations")

location = client.locations.list_locations().body["locations"][0]
