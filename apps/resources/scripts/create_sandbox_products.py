import os
from uuid import uuid4

from square.client import Client

# Square online store
SQUARE_APPLICATION_ID = os.environ.get("SQUARE_APPLICATION_ID")
SQUARE_ACCESS_TOKEN = os.environ.get("SQUARE_ACCESS_TOKEN")
SQUARE_ENVIRONMENT = os.environ.get("SQUARE_ENVIRONMENT", "sandbox")


client = Client(access_token=SQUARE_ACCESS_TOKEN, environment=SQUARE_ENVIRONMENT)

if SQUARE_ENVIRONMENT == "sandbox" and SQUARE_APPLICATION_ID.startswith("sandbox"):
    print("removing old sandbox items")
    catalog_objects = client.catalog.list_catalog().body["objects"]

    result = client.catalog.batch_delete_catalog_objects(
        body={"object_ids": [object["id"] for object in catalog_objects]}
    )

print("getting locations")

location = client.locations.list_locations().body["locations"][0]

print("creating main catalog item")

result = client.catalog.batch_upsert_catalog_objects(
    body={
        "idempotency_key": str(uuid4()),
        "batches": [
            {
                "objects": [
                    {
                        "type": "ITEM",
                        "id": "#thing1",
                        "item_data": {
                            "name": "A things",
                            "description": "a thing, a thing",
                            "abbreviation": "thing?",
                            "available_online": True,
                            "available_for_pickup": False,
                            "available_electronically": True,
                            "product_type": "REGULAR",
                            "skip_modifier_screen": False,
                        },
                    }
                ]
            }
        ],
    }
)

if result.is_success():
    item_id = result.body["objects"][0]["id"]
    print(f"catalog item created {item_id}")
elif result.is_error():
    print(result.errors)
    import pdb

    pdb.set_trace()

print("creating variation (language)")

# Now create version:

result = client.catalog.batch_upsert_catalog_objects(
    body={
        "idempotency_key": str(uuid4()),
        "batches": [
            {
                "objects": [
                    {
                        "type": "ITEM_VARIATION",
                        "id": "#var1",
                        "item_variation_data": {
                            "item_id": item_id,
                            "name": "Version in English",
                            "sku": "TEST1_EN",
                            "pricing_type": "FIXED_PRICING",
                            "price_money": {"amount": 100, "currency": "GBP"},  # 100 = £1.00
                        },
                    }
                ]
            }
        ],
    }
)

if result.is_success():
    # print(result.body)
    variation_id = result.body["objects"][0]["id"]
    print(f"variation {variation_id} created.")
elif result.is_error():
    print(result.errors)
    import pdb

    pdb.set_trace()


#######################################################
# create checkout


result = client.checkout.create_checkout(
    location_id=location["id"],
    body={
        "idempotency_key": str(uuid4()),
        "order": {
            "order": {
                "location_id": location["id"],
                "line_items": [
                    {
                        "quantity": "1",
                        "catalog_object_id": variation_id,
                        "base_price_money": {"amount": 100, "currency": "GBP"},  # 100 = £1.00
                    }
                ],
            },
            "idempotency_key": str(uuid4()),
        },
        "ask_for_shipping_address": False,
    },
)

if result.is_success():
    # print(result.body)
    print("A temporary fake product has been created for you to buy:")
    print(result.body["checkout"]["checkout_page_url"])
elif result.is_error():
    print(result.errors)
