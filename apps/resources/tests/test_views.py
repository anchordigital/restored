import json
from unittest.mock import patch
from uuid import uuid4

from django.core import mail
from django.core.files.base import ContentFile
from django.test import TestCase
from django.urls import reverse, reverse_lazy

from wagtail.core.models import Site

from pages.tests.factories import RestoredSiteTest
from resources import choices
from resources.models import (
    OrderProcessed,
    ResourceDownloadRequest,
    UniqueDownloadCode,
    UniqueDownloadCodeDownload,
)

from .factories import FileFactory, FreeResourceOnPageFactory, ResourcePageFactory


class MockSquareResult:
    def __init__(self, body, is_success=True):
        self.success = is_success
        self.body = body

    def is_success(self):
        return self.success


class TestDownloadForm(RestoredSiteTest):
    url = reverse_lazy("resources:download-request")

    def setUp(self):
        self.unlistedfile = FileFactory.create(title="A downloadable thing")
        self.unlistedfile.file.save("thing.txt", ContentFile("Hello World"))
        self.emptypage = ResourcePageFactory.create()

        self.linked = FreeResourceOnPageFactory.create()
        self.linked.file.file.save("thing2.txt", ContentFile("Hello World!"))

    def test_form_get(self):
        resp = self.client.get(self.url)
        self.assertEqual(resp.status_code, 405)

    def test_form_post_problem_no_email_no_problem(self):
        # Should redirect to the file and save a download request.
        self.linked.resource_page.resource_type = choices.FREE_DOWNLOAD
        resp = self.client.post(
            self.url,
            data={"file": self.linked.file.id, "resource_page": self.linked.resource_page.pk,},
        )
        self.assertRedirects(resp, self.linked.file.url)
        self.assertEqual(ResourceDownloadRequest.objects.count(), 1)

    def test_form_post_problem_email_and_subscribe(self):
        # Should redirect to the file and save a download request.
        self.linked.resource_page.resource_type = choices.FREE_DOWNLOAD
        resp = self.client.post(
            self.url,
            data={
                "file": self.linked.file.id,
                "resource_page": self.linked.resource_page.pk,
                "subscribe_me": "on",
            },
        )
        self.assertRedirects(resp, self.linked.file.url)
        self.assertEqual(ResourceDownloadRequest.objects.count(), 1)
        self.assertTrue(ResourceDownloadRequest.objects.first().subscribe_me)

    def test_form_post_problem_email_and_no_subscribe(self):
        # Should redirect to the file and save a download request.
        self.linked.resource_page.resource_type = choices.FREE_DOWNLOAD
        resp = self.client.post(
            self.url,
            data={"file": self.linked.file.id, "resource_page": self.linked.resource_page.pk,},
        )
        self.assertRedirects(resp, self.linked.file.url)
        self.assertEqual(ResourceDownloadRequest.objects.count(), 1)
        self.assertFalse(ResourceDownloadRequest.objects.first().subscribe_me)

    def test_form_post_problem_no_email_but_public(self):
        # Should redirect to the file and save a download request.
        self.linked.resource_page.resource_type = choices.PUBLIC_DOWNLOAD
        self.linked.resource_page.save()
        resp = self.client.post(
            self.url,
            data={"file": self.linked.file.id, "resource_page": self.linked.resource_page.pk,},
        )
        self.assertRedirects(resp, self.linked.file.url)
        self.assertEqual(ResourceDownloadRequest.objects.count(), 1)

    def test_form_post_problem_not_linked(self):
        resp = self.client.post(
            self.url,
            data={
                "file": self.linked.file.id,
                "resource_page": self.emptypage.pk,
                "email": "test@test.com",
            },
        )
        self.assertEqual(resp.status_code, 200)
        self.assertFalse(resp.context["form"].is_valid())
        self.assertEqual(ResourceDownloadRequest.objects.count(), 0)

    def test_form_post_problem_not_on_page(self):
        resp = self.client.post(
            self.url,
            data={
                "file": self.unlistedfile.id,
                "resource_page": self.linked.resource_page.pk,
                "email": "test@test.com",
            },
        )
        self.assertEqual(resp.status_code, 200)
        self.assertFalse(resp.context["form"].is_valid())
        self.assertEqual(ResourceDownloadRequest.objects.count(), 0)

    def test_form_post_correct(self):
        resp = self.client.post(
            self.url,
            data={
                "file": self.linked.file.id,
                "resource_page": self.linked.resource_page.pk,
                "email": "test@test.com",
            },
        )
        self.assertRedirects(resp, self.linked.file.url)
        self.assertEqual(ResourceDownloadRequest.objects.count(), 1)


class TestUniqueDownloadView(RestoredSiteTest):
    def setUp(self):
        self.file = FileFactory.create(title="A downloadable thing")
        self.file.file.save("thing.txt", ContentFile("Hello World"))

    def test_when_exists(self):
        unique_code = UniqueDownloadCode.objects.create(file=self.file, code="this-is-a-code")
        url = reverse("resources:unique-code-download", args=[unique_code.code])
        resp = self.client.get(url)
        self.assertRedirects(resp, self.file.url)
        self.assertEqual(unique_code.downloads.count(), 1)

    def test_when_doesnt_exist(self):
        url = reverse("resources:unique-code-download", args=["BLAH"])
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 404)
        self.assertEqual(UniqueDownloadCodeDownload.objects.count(), 0)


class WebhookTestCase(TestCase):
    url = reverse_lazy("resources:receive-webhook")

    def setUp(self):
        # we don't want to actually call the square API in tests
        # NOTE: this is mocking the raw square Client, not our wrapper.
        square_api_client = patch("resources.square.Client")
        self.mock_square = square_api_client.start().return_value
        self.addCleanup(square_api_client.stop)

    def post_json(self, obj):
        return self.client.post(self.url, json.dumps(obj), content_type="application/json")


class TestWebHookIncorrectData(WebhookTestCase):
    """
        Not testing logic - just testing that invalid data is caught and reasonable errors
        returned.
    """

    def test_get_not_post(self):
        resp = self.client.get(self.url)
        self.assertEqual(resp.status_code, 405)

    def test_invalid_sig(self):
        resp = self.client.post(self.url, {"things": "go", "here": "more"})
        self.assertContains(resp, "Invalid Signature", status_code=403)

    @patch("resources.views.validate_webhook_signature", return_value=True)
    def test_incorrect_content_type(self, patcher):
        resp = self.client.post(self.url, {"things": "go", "here": "more"})
        self.assertEqual(resp.status_code, 405)

        self.assertContains(resp, "Invalid JSON", status_code=405)

    @patch("resources.views.validate_webhook_signature", return_value=True)
    def test_incorrect_webhook_type(self, patcher):
        resp = self.post_json({"things": "go", "here": "more"})
        self.assertContains(resp, "Unknown Webhook Type", status_code=405)

    @patch("resources.views.validate_webhook_signature", return_value=True)
    def test_incorrect_payload_type(self, patcher):
        resp = self.post_json({"type": "order.updated", "things": "go", "here": "more"})
        self.assertContains(resp, "Not an order", status_code=405)


class TestValidWebhook(WebhookTestCase):
    location_id = "MOCK_LOCATION_ID"

    def setUp(self):
        super().setUp()
        # All tests here assume valid signatures.
        validate_sig = patch("resources.views.validate_webhook_signature", return_value=True)
        validate_sig.start()
        self.addCleanup(validate_sig.stop)

        # Default Payload, correctly formatted and generated in setUp so it's fresh on each test.
        self.order_id = str(uuid4())

        self.payload = {
            "type": "order.updated",
            "data": {
                "type": "order",
                "id": self.order_id,
                "object": {"order_updated": {"state": "OPEN", "location_id": self.location_id}},
            },
        }

        # Add a base site URL to confirm that works correctly in emails, etc.
        site = Site.objects.first()
        site.hostname = "testing_site_url.com"
        site.save()

    # Some helper methods, as mocking this deep is sooo verbose:

    def create_mock_order(self, order_id):
        self.mock_square.orders.batch_retrieve_orders.return_value = MockSquareResult(
            {
                "orders": [
                    {
                        "line_items": [{"catalog_object_id": order_id}],
                        "customer_id": "CUSTOMER_ID",
                    }
                ]
            }
        )

    def create_mock_catalog_items_with_sku(self, *skus):
        self.mock_square.catalog.batch_retrieve_catalog_objects.return_value = MockSquareResult(
            {
                "objects": [
                    {"type": "ITEM_VARIATION", "item_variation_data": {"sku": sku}} for sku in skus
                ]
            }
        )

    def create_mock_customer(self, email_address):
        self.mock_square.customers.retrieve_customer.return_value = MockSquareResult(
            {"customer": {"email_address": email_address}}
        )

    # Finally some actual tests!

    def test_valid_no_skus_found(self):
        resp = self.post_json(self.payload)
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(UniqueDownloadCode.objects.count(), 0)
        self.assertEqual(len(mail.outbox), 0)

    def test_single_valid_sku_but_not_a_file_here(self):
        # This could be because Square is telling us about a sale of a non-digital product.

        valid_sku = "VALID_SKU_FOR_SALE"
        valid_order_id = "BANANA_IN_SQUARE"

        # Mock responses from square:
        self.create_mock_order(valid_order_id)
        self.create_mock_catalog_items_with_sku(valid_sku)
        self.create_mock_customer("test@test.com")

        resp = self.post_json(self.payload)

        self.mock_square.orders.batch_retrieve_orders.assert_called_once_with(
            self.location_id, {"order_ids": [self.order_id]}
        )
        self.mock_square.catalog.batch_retrieve_catalog_objects.assert_called_once_with(
            {"object_ids": [valid_order_id]}
        )

        self.assertEqual(resp.status_code, 200)
        self.assertEqual(UniqueDownloadCode.objects.count(), 0)
        self.assertEqual(len(mail.outbox), 0)

    def test_single_valid_sku_we_can_send(self):
        valid_sku = "VALID_SKU_FOR_SALE"
        valid_order_id = "BANANA_IN_SQUARE"
        email_address = "test_person@test.com"

        # Here's a file to be sold
        file = FileFactory.create(sku=valid_sku)
        file.file.save("thing.txt", ContentFile("Hello World"))

        # Mock responses from square:
        self.create_mock_order(valid_order_id)
        self.create_mock_catalog_items_with_sku(valid_sku)
        self.create_mock_customer(email_address)

        resp = self.post_json(self.payload)

        self.mock_square.orders.batch_retrieve_orders.assert_called_once_with(
            self.location_id, {"order_ids": [self.order_id]}
        )
        self.mock_square.catalog.batch_retrieve_catalog_objects.assert_called_once_with(
            {"object_ids": [valid_order_id]}
        )

        self.assertEqual(resp.status_code, 200)
        self.assertEqual(UniqueDownloadCode.objects.count(), 1)
        self.assertEqual(len(mail.outbox), 1)

        unique_code = UniqueDownloadCode.objects.first().code
        unique_url = "https://testing_site_url.com" + reverse(
            "resources:unique-code-download", args=[unique_code]
        )
        self.assertIn(unique_url, mail.outbox[0].body)

        # and for good measure, let's check the code url actually works:
        # real_end = self.client.get(file.file.url).re
        resp = self.client.get(unique_url)
        self.assertRedirects(resp, file.url)

        # check order logging:
        self.assertEqual(OrderProcessed.objects.filter(state=choices.ORDER_COMPLETED).count(), 1)

    def test_single_valid_sku_we_can_send_with_no_customer_id(self):
        valid_sku = "VALID_SKU_FOR_SALE"
        valid_order_id = "BANANA_IN_SQUARE"
        email_address = "test_person@test.com"

        # Here's a file to be sold
        file = FileFactory.create(sku=valid_sku)
        file.file.save("thing.txt", ContentFile("Hello World"))

        # Mock responses from square:
        # rather than self.create_mock_order(valid_order_id)
        # we'll return info w/o a customer_id, but with tenders

        self.mock_square.orders.batch_retrieve_orders.return_value = MockSquareResult(
            {
                "orders": [
                    {
                        "line_items": [{"catalog_object_id": valid_order_id}],
                        "tenders": [{"payment_id": "PAYMENT_ID"}],
                    }
                ]
            }
        )

        self.create_mock_catalog_items_with_sku(valid_sku)

        # Rather than self.create_mock_customer(email_address)
        # we'll return an unsuccessful result:
        self.mock_square.customers.retrieve_customer.return_value = MockSquareResult(
            {}, is_success=False
        )

        # And instead, we'll return payment info:
        self.mock_square.payments.get_payment.return_value = MockSquareResult(
            {"payment": {"buyer_email_address": email_address}}
        )

        resp = self.post_json(self.payload)

        self.mock_square.payments.get_payment.assert_called()

        self.mock_square.orders.batch_retrieve_orders.assert_called_once_with(
            self.location_id, {"order_ids": [self.order_id]}
        )
        self.mock_square.catalog.batch_retrieve_catalog_objects.assert_called_once_with(
            {"object_ids": [valid_order_id]}
        )

        self.assertEqual(resp.status_code, 200)
        self.assertEqual(UniqueDownloadCode.objects.count(), 1)
        self.assertEqual(len(mail.outbox), 1)

        unique_code = UniqueDownloadCode.objects.first().code
        unique_url = "https://testing_site_url.com" + reverse(
            "resources:unique-code-download", args=[unique_code]
        )
        self.assertIn(unique_url, mail.outbox[0].body)

        # and for good measure, let's check the code url actually works:
        # real_end = self.client.get(file.file.url).re
        resp = self.client.get(unique_url)
        self.assertRedirects(resp, file.url)

        # check order logging:
        self.assertEqual(OrderProcessed.objects.filter(state=choices.ORDER_COMPLETED).count(), 1)

    def test_valid_skus_multiple_files_found(self):
        # This isn't a problem - all files with that SKU get unique codes made and sent in
        # that email.

        valid_sku = "VALID_SKU_FOR_SALE"
        valid_order_id = "BANANA_IN_SQUARE"
        email_address = "test_person@test.com"

        # Here's a file to be sold
        file = FileFactory.create(sku=valid_sku)
        file.file.save("thing.txt", ContentFile("Hello World"))

        FileFactory.create(sku=valid_sku)
        file.file.save("thing2.txt", ContentFile("¡Hola Mundo!"))

        # Mock responses from square:
        self.create_mock_order(valid_order_id)
        self.create_mock_catalog_items_with_sku(valid_sku)
        self.create_mock_customer(email_address)

        resp = self.post_json(self.payload)
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(UniqueDownloadCode.objects.count(), 2)

        # Check email contains all download links:
        for code in UniqueDownloadCode.objects.all():
            unique_url = reverse("resources:unique-code-download", args=[code.code])
            self.assertIn(unique_url, mail.outbox[0].body)

        # check order logging:
        self.assertEqual(OrderProcessed.objects.filter(state=choices.ORDER_COMPLETED).count(), 1)

    def test_multiple_valid_skus_found(self):
        valid_skus = ["VALID_SKU_FOR_SALE", "SKU2", "SKU_42"]
        valid_order_id = "BANANA_IN_SQUARE"
        email_address = "test_person@test.com"

        files = []
        for sku in valid_skus:
            # Here's a file to be sold
            file = FileFactory.create(sku=sku)
            file.file.save(f"{sku}.txt", ContentFile(f"{sku} is the file"))
            files.append(file)

        # Mock responses from square:
        self.create_mock_order(valid_order_id)
        self.create_mock_catalog_items_with_sku(*valid_skus)
        self.create_mock_customer(email_address)

        resp = self.post_json(self.payload)
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(UniqueDownloadCode.objects.count(), 3)
        self.assertEqual(len(mail.outbox), 1)

        # Check email contains all download links:
        for code in UniqueDownloadCode.objects.all():
            unique_url = reverse("resources:unique-code-download", args=[code.code])
            self.assertIn(unique_url, mail.outbox[0].body)

    def test_some_valid_skus_found(self):
        # But not all.
        valid_skus = ["VALID_SKU_FOR_SALE", "SKU2", "SKU_42"]
        valid_order_id = "BANANA_IN_SQUARE"
        email_address = "test_person@test.com"

        files = []
        for sku in valid_skus[:2]:
            # Here's a file to be sold
            file = FileFactory.create(sku=sku)
            file.file.save(f"{sku}.txt", ContentFile(f"{sku} is the file"))
            files.append(file)

        # Mock responses from square:
        self.create_mock_order(valid_order_id)
        self.create_mock_catalog_items_with_sku(*valid_skus)
        self.create_mock_customer(email_address)

        resp = self.post_json(self.payload)
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(UniqueDownloadCode.objects.count(), 2)
        self.assertEqual(len(mail.outbox), 1)

        # Check email contains all download links:
        for code in UniqueDownloadCode.objects.all():
            unique_url = reverse("resources:unique-code-download", args=[code.code])
            self.assertIn(unique_url, mail.outbox[0].body)

    def test_recived_twice(self):
        self.test_single_valid_sku_we_can_send()
        self.assertEqual(OrderProcessed.objects.filter(state=choices.ORDER_COMPLETED).count(), 1)
        self.assertEqual(UniqueDownloadCode.objects.count(), 1)

        resp = self.post_json(self.payload)
        self.assertEqual(resp.status_code, 200)

        self.assertEqual(UniqueDownloadCode.objects.count(), 1)
        self.assertEqual(OrderProcessed.objects.filter(state=choices.ORDER_COMPLETED).count(), 1)

    def test_recived_cancellation(self):
        self.test_single_valid_sku_we_can_send()
        self.assertEqual(OrderProcessed.objects.filter(state=choices.ORDER_COMPLETED).count(), 1)
        self.assertEqual(UniqueDownloadCode.objects.count(), 1)

        # Create another unique code, just to confirm that deleteing some doesn't delete all...
        other_file = FileFactory.create(sku="NADA")
        other_code = UniqueDownloadCode.objects.create(file=other_file)
        self.assertEqual(UniqueDownloadCode.objects.count(), 2)

        # Send a Cancellation webhook:
        self.payload["data"]["object"]["order_updated"]["state"] = choices.ORDER_CANCELLED
        resp = self.post_json(self.payload)
        self.assertEqual(resp.status_code, 200)

        # Check the state is updated, and the codes deleted:
        self.assertEqual(UniqueDownloadCode.objects.count(), 1)
        self.assertEqual(UniqueDownloadCode.objects.first().pk, other_code.pk)
        self.assertEqual(OrderProcessed.objects.filter(state=choices.ORDER_COMPLETED).count(), 0)

    def test_multiple_webhook_recieved(self):
        valid_sku = "VALID_SKU_FOR_SALE"
        valid_order_id = "BANANA_IN_SQUARE"
        email_address = "test_person@test.com"

        # Here's a file to be sold
        file = FileFactory.create(sku=valid_sku)
        file.file.save("thing.txt", ContentFile("Hello World"))

        # Mock responses from square:
        self.create_mock_order(valid_order_id)
        self.create_mock_catalog_items_with_sku(valid_sku)
        self.create_mock_customer(email_address)

        resp = self.post_json(self.payload)

        self.mock_square.orders.batch_retrieve_orders.assert_called_once_with(
            self.location_id, {"order_ids": [self.order_id]}
        )
        self.mock_square.catalog.batch_retrieve_catalog_objects.assert_called_once_with(
            {"object_ids": [valid_order_id]}
        )

        self.assertEqual(resp.status_code, 200)
        self.assertEqual(UniqueDownloadCode.objects.count(), 1)
        self.assertEqual(len(mail.outbox), 1)

        unique_code = UniqueDownloadCode.objects.first().code
        unique_url = reverse("resources:unique-code-download", args=[unique_code])
        self.assertIn(unique_url, mail.outbox[0].body)

        # and for good measure, let's check the code url actually works:
        # real_end = self.client.get(file.file.url).re
        resp = self.client.get(unique_url)
        self.assertRedirects(resp, file.url)

        # check order logging:
        self.assertEqual(OrderProcessed.objects.filter(state=choices.ORDER_COMPLETED).count(), 1)
