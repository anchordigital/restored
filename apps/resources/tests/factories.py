import factory
import wagtail_factories
from factory.django import DjangoModelFactory

from resources import models


class MediaTypeFactory(DjangoModelFactory):
    title = factory.Faker("name")
    slug = factory.Sequence(lambda n: f"Resource medium {n}")

    class Meta:
        model = models.MediaType


class LanguageFactory(DjangoModelFactory):
    title = factory.Sequence(lambda n: f"Language no. {n}")

    class Meta:
        model = models.Language


class FileFactory(DjangoModelFactory):
    language = factory.SubFactory(LanguageFactory)

    # TODO - SKU unique stuff???

    class Meta:
        model = models.File


class ResourcePageFactory(wagtail_factories.PageFactory):
    title = factory.Faker("text")

    class Meta:
        model = models.ResourcePage


class ResourceIndexPageFactory(wagtail_factories.PageFactory):
    title = factory.Faker("text")

    class Meta:
        model = models.ResourceIndexPage


class FreeResourceOnPageFactory(DjangoModelFactory):
    file = factory.SubFactory(FileFactory)
    resource_page = factory.SubFactory(ResourcePageFactory)

    class Meta:
        model = models.FreeResourceOnPage


class ResourceDownloadRequestFactory(DjangoModelFactory):
    file = factory.SubFactory(FileFactory)

    class Meta:
        model = models.ResourceDownloadRequest
