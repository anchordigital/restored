from django.core.files.base import ContentFile
from django.urls import reverse

from wagtail.tests.utils import WagtailPageTests

from pages.tests.factories import RestoredSiteTest

from .factories import (
    FileFactory,
    LanguageFactory,
    MediaTypeFactory,
    ResourceDownloadRequestFactory,
    ResourceIndexPageFactory,
    ResourcePageFactory,
)


class TestResourceMediumModel(WagtailPageTests):
    def assertAdminViewsOK(self, pk=None):
        resp = self.client.get(reverse("resources_mediatype_modeladmin_index"))
        self.assertEqual(resp.status_code, 200)

        resp = self.client.get(reverse("resources_mediatype_modeladmin_create"))
        self.assertEqual(resp.status_code, 200)

        if pk:
            resp = self.client.get(reverse("resources_mediatype_modeladmin_edit", args=[pk]))
            self.assertEqual(resp.status_code, 200)

    def test_admin_views_with_no_models(self):
        self.assertAdminViewsOK()

    def test_init(self):
        medium = MediaTypeFactory.create()
        self.assertIsNotNone(medium.pk)

        self.assertAdminViewsOK(pk=medium.pk)


class TestLanguageModel(WagtailPageTests):
    def assertAdminViewsOK(self, pk=None):
        resp = self.client.get(reverse("resources_language_modeladmin_index"))
        self.assertEqual(resp.status_code, 200)

        resp = self.client.get(reverse("resources_language_modeladmin_create"))
        self.assertEqual(resp.status_code, 200)

        if pk:
            resp = self.client.get(reverse("resources_language_modeladmin_edit", args=[pk]))
            self.assertEqual(resp.status_code, 200)

    def test_admin_views_with_no_models(self):
        self.assertAdminViewsOK()

    def test_init(self):
        language = LanguageFactory.create()
        self.assertIsNotNone(language.pk)
        self.assertAdminViewsOK(pk=language.pk)


class TestFileFactory(WagtailPageTests):
    def test_init(self):
        file = FileFactory.create()
        self.assertIsNotNone(file.pk)


class TestResourceDownloadRequest(WagtailPageTests):
    def assertAdminViewsOK(self, pk=None):
        resp = self.client.get(reverse("resources_resourcedownloadrequest_modeladmin_index"))
        self.assertEqual(resp.status_code, 200)

        resp = self.client.get(reverse("resources_resourcedownloadrequest_modeladmin_create"))
        self.assertEqual(resp.status_code, 200)

        if pk:
            resp = self.client.get(
                reverse("resources_resourcedownloadrequest_modeladmin_edit", args=[pk])
            )
            self.assertEqual(resp.status_code, 200)

    def test_admin_views_with_no_models(self):
        self.assertAdminViewsOK()

    def test_model_with_no_email_content(self):
        file = FileFactory.create(title="A downloadable thing")
        file.file.save("thing.txt", ContentFile("Hello World"))
        ResourceDownloadRequestFactory.create(file=file)
        self.assertAdminViewsOK()

    def test_model_with_file_content(self):
        file = FileFactory.create(title="A downloadable thing")
        file.file.save("thing.txt", ContentFile("Hello World"))
        request = ResourceDownloadRequestFactory.create(file=file, email="blah@blah.com")
        self.assertAdminViewsOK(pk=request.pk)


class TestResourceIndexPage(RestoredSiteTest):
    def setUp(self):
        super().setUp()
        self.page = ResourceIndexPageFactory.create(parent=self.homepage)

    def test_empty_indexpage(self):
        resp = self.client.get(self.page.get_url())
        self.assertEqual(resp.status_code, 200)


class TestResourcePage(RestoredSiteTest):
    def setUp(self):
        super().setUp()
        self.parentpage = ResourceIndexPageFactory.create(parent=self.homepage)

    def test_empty_page(self):
        page = ResourcePageFactory.create(parent=self.parentpage)
        resp = self.client.get(page.get_url())
        self.assertEqual(resp.status_code, 200)
