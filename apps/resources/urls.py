from django.urls import path

from . import views

app_name = "resources"
urlpatterns = [
    path("download/", views.CreateResourceDownloadRequest.as_view(), name="download-request"),
    path("download/uid/<slug:code>", views.download_by_unique_code, name="unique-code-download"),
    path("webhook/", views.receive_webhook, name="receive-webhook"),
]
