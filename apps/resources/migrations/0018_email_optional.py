# Generated by Django 2.2.13 on 2020-08-10 08:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('resources', '0017_greyscale_and_sidebar_optional'),
    ]

    operations = [
        migrations.AlterField(
            model_name='resourcedownloadrequest',
            name='email',
            field=models.EmailField(blank=True, max_length=254, null=True),
        ),
        migrations.AlterField(
            model_name='resourcepage',
            name='resource_type',
            field=models.CharField(choices=[('public_download', 'Public, no email address required'), ('free_download', 'Free Download, after submitting email'), ('paid_download', 'Payment required for download'), ('paid_nondigital', 'Non-digital product'), ('external_link', 'External Link (not our store)')], default='free_download', max_length=40),
        ),
    ]
