from django.db import models
from django.utils import timezone
from django.utils.functional import cached_property

import django_filters
from modelcluster.fields import ParentalKey, ParentalManyToManyField
from wagtail.admin.edit_handlers import (
    FieldPanel,
    HelpPanel,
    InlinePanel,
    MultiFieldPanel,
    ObjectList,
    TabbedInterface,
)
from wagtail.contrib.settings.models import BaseSetting, register_setting
from wagtail.documents.edit_handlers import DocumentChooserPanel
from wagtail.documents.models import AbstractDocument, Document
from wagtail.search import index

from pages.models import ArchivablePage, ListPage

from wagtail.admin.panels import PageChooserPanel

from . import choices

################################################################################
# Categorization


class MediaType(models.Model):
    """
        PDF Document, Audio file, banner, etc...
    """

    title = models.CharField(max_length=100, db_index=True)
    slug = models.SlugField(unique=True)

    class Meta:
        ordering = ("title",)
        verbose_name = "Resource Medium Type"
        verbose_name_plural = "Resource Media Types"

    def __str__(self):
        return self.title


class Language(models.Model):
    title = models.CharField(max_length=100, db_index=True)

    class Meta:
        ordering = ("title",)

    def __str__(self):
        return self.title


################################################################################
# Actual Documents / Resources for download (or physical requesting...)


class File(AbstractDocument):
    # All additional fields must (alas) be blank=True, or else wagtail fails to allow uploads.
    language = models.ForeignKey(Language, null=True, blank=True, on_delete=models.CASCADE)

    file_info = models.CharField(
        max_length=100, blank=True, help_text="Optional, eg. Low Quality, Colour, Black and White"
    )
    sku = models.CharField(
        max_length=100,
        verbose_name="SKU",
        help_text="Unique code for ordering, as referenced by the online store.",
        # These must be set to allow wagtail admin to upload new ones:
        blank=True,
        null=True,
    )

    admin_form_fields = Document.admin_form_fields + ("sku", "language", "file_info")
    search_fields = Document.search_fields + [
        index.SearchField("sku"),
        index.SearchField("language"),
        index.SearchField("file_info"),
    ]

    class Meta:
        ordering = ("language",)

    def __str__(self):
        return "%s (%s)" % (self.file_info, self.language)


################################################################################
# How those files / resources are served to site visitors:


class FreeResourceOnPage(models.Model):
    """
        Links a File (uploaded document) with a ResourcePage.
    """

    # xref:
    file = models.ForeignKey(File, null=True, blank=True, on_delete=models.CASCADE)
    resource_page = ParentalKey("ResourcePage", related_name="free_resources")

    panels = [
        DocumentChooserPanel("file"),
    ]


class UniqueDownloadCode(models.Model):
    """
        Sent to users (or manually created (at conferences)) to let them download a file.
        When the online store completes a purchase, it should fire a webhook here which
        creates one of these and sents it to the buyer's email address.
    """

    file = models.ForeignKey(File, on_delete=models.CASCADE)
    reference = models.CharField(
        max_length=200,
        null=True,
        blank=False,
        help_text="Email address (if purchased) or other reference information.",
    )
    code = models.SlugField(verbose_name="Unique Download Code", unique=True)
    order = models.ForeignKey("OrderProcessed", on_delete=models.CASCADE, null=True, default=None)

    panels = [
        FieldPanel("code"),
        FieldPanel("reference"),
        DocumentChooserPanel("file"),
    ]


################################################################
# The Resource Pages / List Pages models:


class ResourcePage(ArchivablePage):
    """
        A resources page displays one 'resource'.
        They can have 1 "buy"/external link, and many free files linked to them.
        Depending on the resource type, free resources will either require an email
        address to download, or else will link directly to the file URL.
    """

    # fields:
    location = ParentalManyToManyField("categories.Location", blank=True)
    topic = ParentalManyToManyField("categories.Topic", blank=True)
    author = models.ForeignKey("accounts.User", null=True, blank=True, on_delete=models.SET_NULL)

    resource_type = models.CharField(
        max_length=40, choices=choices.RESOURCE_CHOICES, default=choices.FREE_DOWNLOAD
    )
    resource_medium = models.ForeignKey(
        MediaType, null=True, blank=True, on_delete=models.SET_NULL
    )

    external_url = models.URLField(
        verbose_name="The link to the page on the online store, or other external web page.",
        blank=True,
    )

    # options:
    parent_page_types = ["resources.ResourceIndexPage"]
    template = "resources/resource_detail.html"

    # admin layout:
    content_panels = ArchivablePage.content_panels + [
        FieldPanel("location"),
        FieldPanel("topic"),
        FieldPanel("author"),
    ]
    resource_panels = [
        FieldPanel("resource_type"),
        FieldPanel("resource_medium"),
        FieldPanel("external_url"),
        MultiFieldPanel(
            [
                HelpPanel(
                    "Paid resources are configured in the online store – "
                    "any resources listed here will only be shown if the Resource Type "
                    "is Public or Free."
                ),
                InlinePanel("free_resources"),
            ],
            heading="Free Resource files / variations",
        ),
    ]
    edit_handler = TabbedInterface(
        [
            ObjectList(content_panels, heading="Page Content"),
            ObjectList(resource_panels, heading="Resources / Files"),
            ObjectList(ArchivablePage.promote_panels, heading="Promote"),
            ObjectList(ArchivablePage.settings_panels, heading="Settings", classname="settings"),
        ]
    )

    @cached_property
    def regions(self):
        # This must be done here, rather than as a QuerySet method, as Page Preview
        # uses FakeQuerySet.
        return self.location.filter(location_type="R")

    @cached_property
    def countries(self):
        # This must be done here, rather than as a QuerySet method, as Page Preview
        # uses FakeQuerySet.
        return self.location.filter(location_type="C")

    def related_by_topic(self):
        related = (
            ResourcePage.objects.live()
            .public()
            .exclude(pk=self.pk)
            .filter(topic__in=self.topic.all())
            .order_by("pk")
            .annotate(score=models.Count("pk"))
            .order_by("-score")[:5]
        )
        if not related:
            return self.get_siblings()[:5]
        return related


class ResourcePageFilter(django_filters.FilterSet):
    """
        Filter options for filtering resources on a ResourceIndexPage
    """

    type = django_filters.CharFilter(field_name="resource_type__slug")
    topic = django_filters.CharFilter(field_name="topic__slug")
    location = django_filters.CharFilter(field_name="location__slug")

    class Meta:
        model = ResourcePage
        fields = ["type", "location", "topic"]


class ResourceIndexPage(ListPage):
    """
        List a set of (child page) resources.
        There will be one main 'free resources' type page, and then in separate sections of the
        site others.  So different audiences / groups can have access to different resources.
        E.g. a Church Leaders section, a survivors section, a staff section, etc...
    """

    template = "resources/resource_list.html"
    subpage_types = ["ResourcePage"]
    filterset = ResourcePageFilter

    def get_queryset(self):
        return ResourcePage.objects.descendant_of(self).live().order_by("-first_published_at")


################################################################################
# Logging downloads / requests / etc.


class ResourceDownloadRequest(models.Model):
    """
        For Resources that require an email address to download, one of these
        is created for each when they fill in the form.
    """

    file = models.ForeignKey(File, on_delete=models.CASCADE)
    resource_page = models.ForeignKey(ResourcePage, null=True, on_delete=models.CASCADE)
    first_name = models.CharField("First name or initial",null=True,  max_length=100)
    surname = models.CharField(null=True, max_length=100)
    email = models.EmailField(null=True, blank=True)
    subscribe_me = models.BooleanField(
        "I'd like to hear more in the future.", blank=True, default=False
    )
    date = models.DateTimeField(default=timezone.now, db_index=True)

    class Meta:
        ordering = ("-date",)

    def __str__(self):
        return "%s %s - %s" % (self.email, self.file.title, self.date)


class UniqueDownloadCodeDownload(models.Model):
    """
        Log of one download via a unique code.
    """

    unique_code = models.ForeignKey(
        UniqueDownloadCode, on_delete=models.CASCADE, related_name="downloads"
    )
    timestamp = models.DateTimeField(auto_now_add=True)


class ResourcePrintedCopyRequest(models.Model):
    # resource = models.ForeignKey(Resource, on_delete=models.CASCADE)
    resource_file = models.ForeignKey(File, on_delete=models.CASCADE)
    first_name = models.CharField("First name or initial", max_length=100)
    surname = models.CharField(max_length=100)
    email = models.EmailField()
    street_address = models.CharField(max_length=200)
    address_line_2 = models.CharField(max_length=200)
    city = models.CharField("City/Town", max_length=200)
    postal_code = models.CharField(max_length=20)
    date = models.DateTimeField(default=timezone.now, db_index=True)

    class Meta:
        ordering = ("-date",)

    def __str__(self):
        return "%s %s - %s" % (self.first_name, self.surname, self.date)


class OrderProcessed(models.Model):
    state = models.CharField(max_length=100, blank=True, choices=choices.ORDER_STATES)
    order_id = models.CharField(max_length=100, blank=True, unique=True)
    timestamp = models.DateTimeField(auto_now=True)


#################################################################################
# Settings


@register_setting(icon="code")
class SquareWebHookSettings(BaseSetting):
    signature_key = models.CharField(
        max_length=100,
        verbose_name="WebHook Signature Key",
        help_text="Get this when you set up the web-hook on https://developer.squareup.com/apps/",
    )
    email_top = models.TextField(
        verbose_name="Top paragraph",
        help_text="Text at the top of emails containing unique codes",
        default="""
    Thank you for purchasing resources from Restored!
    You can access your purchase at the following URLs:
    """,
    )
    email_foot = models.TextField(
        verbose_name="End paragraph",
        help_text="Text at the end of emails containing unique codes",
        default="""
        Thank you again,

        The Restored Team
    """,
    )

    panels = [
        MultiFieldPanel(
            [
                HelpPanel(
                    "When customers buy new resources, they get sent an email with unique links"
                    " to their resources so they can download them."
                ),
                FieldPanel("email_top"),
                FieldPanel("email_foot"),
            ],
            heading="Emails send to customers with unique links",
        ),
        MultiFieldPanel([FieldPanel("signature_key")], heading="Advanced Settings",),
    ]

    class Meta:
        verbose_name = "Square online shop integration"


@register_setting(icon="code")
class ResourceSettings(BaseSetting):
    thankyou_page = models.ForeignKey(
        'wagtailcore.Page',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
    )

    email_top = models.TextField(
        verbose_name="Top paragraph",
        help_text="Text at the top of emails containing download link",
        default="""
    Thank you for purchasing resources from Restored!
    You can access your purchase at the following URLs: 
    """,
    )
    email_foot = models.TextField(
        verbose_name="End paragraph",
        help_text="Text at the end of emails containing unique codes",
        default="""
        Thank you again,

        The Restored Team
    """,
    )

    panels = [
        MultiFieldPanel(
            [
                HelpPanel(
                    "When customers download resources, they get sent an email with their links"
                    " to their resources so they can download them."
                ),
                FieldPanel("email_top"),
                FieldPanel("email_foot"),
                PageChooserPanel('thankyou_page'),
            ],
        ),
    ]

    class Meta:
        verbose_name = "Download Resource settings"
