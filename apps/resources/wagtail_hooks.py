from django.urls import reverse
from django.utils.html import format_html

from import_export import resources
from wagtail.contrib.modeladmin.options import ModelAdmin, ModelAdminGroup, modeladmin_register
from wagtail_exportcsv.admin import ExportModelAdminMixin

from .models import (
    Language,
    MediaType,
    ResourceDownloadRequest,
    ResourcePrintedCopyRequest,
    UniqueDownloadCode,
)


class MediaTypeAdmin(ModelAdmin):
    model = MediaType
    list_display = (
        "title",
        "slug",
    )


class LanguageAdmin(ModelAdmin):
    model = Language
    list_display = ("title",)


class UniqueDownloadCodeAdmin(ModelAdmin):
    model = UniqueDownloadCode
    list_display = ("file", "reference", "code_with_url", "download_count")

    def code_with_url(self, obj):
        return format_html(
            '<a href="{url}">{text}</a>',
            url=reverse("resources:unique-code-download", args=[obj.code]),
            text=f"{obj.code}",
        )

    code_with_url.short_description = "Unique Code (and Link)"

    def download_count(self, obj):
        return obj.downloads.count()


class ResourceDownloadRequestResource(resources.ModelResource):
    class Meta:
        model = ResourceDownloadRequest
        fields = ("first_name", "surname", "email", "subscribe_me", "date", "file__title")


class ResourceDownloadRequestAdmin(ExportModelAdminMixin, ModelAdmin):
    model = ResourceDownloadRequest
    list_display = ("first_name", "surname", "email", "subscribe_me", "which_resource", "date")
    resource_class = ResourceDownloadRequestResource

    def which_resource(self, obj):
        return format_html(
            '<a href="{url}">{text}</a>',
            url=reverse("wagtaildocs:edit", args=[obj.file.id]),
            text=f"{obj.file.title} ({obj.file.sku})",
        )

    which_resource.short_description = "Document (SKU)"


class ResourcePrintedCopyRequestResource(resources.ModelResource):
    class Meta:
        model = ResourcePrintedCopyRequest
        fields = "__all__"


class ResourcePrintedCopyRequestAdmin(ExportModelAdminMixin, ModelAdmin):
    model = ResourcePrintedCopyRequest
    list_display = ("resource", "email", "date")
    resource_class = ResourcePrintedCopyRequestResource


class ResourcesGroup(ModelAdminGroup):
    menu_label = "Resources"
    menu_icon = "folder-open-inverse"
    menu_order = 200
    items = (
        MediaTypeAdmin,
        LanguageAdmin,
        ResourceDownloadRequestAdmin,
        ResourcePrintedCopyRequestAdmin,
        UniqueDownloadCodeAdmin,
    )


modeladmin_register(ResourcesGroup)
