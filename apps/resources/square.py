"""
    All operations related to square (the online store) are in here.
    Ones which need a valid square client are wrapped in the SquareClient class,
    which should keep sessions/connections to a minimum.

    Required env vars:
        SQUARE_APPLICATION_ID (when you register the site to be an 'Application')
        SQUARE_ACCESS_TOKEN (used to authenticate with the API)
        SQUARE_ENVIRONMENT='sandbox' (or 'production')

    To process webhooks, you'l also need a webhook signature key, which you get
    when you set up the webhook on Square, and need to set in the wagtail admin settings.

    Square links:
        - Developer docs: https://developer.squareup.com/docs
        - Python SDK (and docs): https://github.com/square/square-python-sdk/
        - API Explorer: https://developer.squareup.com/explorer/square/
        - Developer Dashboard: https://developer.squareup.com/apps
        - Test values (fake cards, etc): https://developer.squareup.com/docs/testing/test-values

"""
from django.conf import settings

from square.client import Client


class SquareClient:
    def __init__(self):
        self.client = Client(
            access_token=settings.SQUARE_ACCESS_TOKEN, environment=settings.SQUARE_ENVIRONMENT
        )

    def _get_skus_from_line_item_uids(self, uids):
        skus = []
        result = self.client.catalog.batch_retrieve_catalog_objects({"object_ids": uids})
        if result.is_success():
            for object in result.body.get("objects", []):
                if object["type"] == "ITEM_VARIATION":
                    if sku := object.get("item_variation_data", {}).get("sku"):
                        skus.append(sku)
                if object["type"] == "ITEM":
                    if sku := object.get("item_data", {}).get("sku"):
                        skus.append(sku)

        return skus

    def get_orders(self, order_id, location_id):
        """
            Retreive a list of orders that match this order ID.
            It *should* just be 1 only, but the square API only has access with a
            'batch_retrieve_orders', so technically could be multiple.  I dunno why.
        """
        result = self.client.orders.batch_retrieve_orders(location_id, {"order_ids": [order_id]})
        if result.is_success():
            return result.body.get("orders", [])
        return []

    def get_skus_from_orders(self, orders):
        """
            given an order id from a webhook, return a list of all SKUs that have been ordered
        """
        uids = []
        for order in orders:
            for item in order.get("line_items", []):
                uids.append(item["catalog_object_id"])

        return self._get_skus_from_line_item_uids(uids)

    def get_email_address_from_orders(self, orders):
        """
            given an order ID from a webhook, return a list of email addresses of customers
            who bought that order - should just be 1, really, but square API gives a list.
        """
        email_addresses = set()
        for order in orders:
            if "customer_id" in order:
                result = self.client.customers.retrieve_customer(order.get("customer_id"))
                if result.is_success():
                    email_addresses.add(result.body.get("customer", {}).get("email_address"))
            for tender in order.get("tenders", []):
                # try to get email address from customer_id, if supplied:
                result = self.client.customers.retrieve_customer(tender.get("customer_id"))
                if result.is_success():
                    email_addresses.add(result.body.get("customer", {}).get("email_address"))

                # also try to get from payment id, if supplied:
                result = self.client.payments.get_payment(tender["payment_id"])
                if result.is_success():
                    email_addresses.add(result.body.get("payment", {}).get("buyer_email_address"))

        # deduplicate and return only non-None addresses:
        return [address for address in email_addresses if address]
