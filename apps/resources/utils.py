from django.core.mail import mail_managers
from django.template.loader import render_to_string

# TODO: implement this when implementing mailing lists
# from mailinglists import lib

# def subscribe_lists(form):
#     lib.subscribe(form)


def printed_copy_notification(printed_copy_request):
    message = render_to_string(
        "resources/printed_copy_email.txt", {"printed_copy_request": printed_copy_request,}
    )
    mail_managers(subject="Printed copy request", message=message)
