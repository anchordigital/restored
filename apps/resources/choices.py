# The different types of resources available.

PUBLIC_DOWNLOAD = "public_download"
FREE_DOWNLOAD = "free_download"
PAID_DOWNLOAD = "paid_download"
PAID_NON_DIGITAL = "paid_nondigital"  # entirely processed by the online store.
EXTERNAL_LINK = "external_link"  # Possibly free, or not, no idea, just send visitors there.

RESOURCE_CHOICES = (
    (PUBLIC_DOWNLOAD, "Public, no email address required"),
    (FREE_DOWNLOAD, "Free Download, after submitting email"),
    (PAID_DOWNLOAD, "Payment required for download"),
    (PAID_NON_DIGITAL, "Non-digital product"),
    (EXTERNAL_LINK, "External Link (not our store)"),
)

ORDER_COMPLETED = "COMPLETED"
ORDER_CANCELLED = "CANCELLED"

ORDER_STATES = (
    (ORDER_COMPLETED, "Completed"),
    (ORDER_CANCELLED, "Cancelled"),
)
