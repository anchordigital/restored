from django.shortcuts import HttpResponse, get_object_or_404, redirect
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST
from django.views.generic import CreateView

from django.conf import settings
from django.core import mail
from django.template.loader import get_template

from mailchimp3 import MailChimp
from django.core import mail

from .forms import DownloadResourceForm
from .models import (
    ResourceDownloadRequest,
    SquareWebHookSettings,
    ResourceSettings,
    UniqueDownloadCode,
    UniqueDownloadCodeDownload,
)
from .process_webhook import InvalidWebhookPayload, process_webhook, validate_webhook_signature


class CreateResourceDownloadRequest(CreateView):
    template_name = "resources/download_request.html"
    model = ResourceDownloadRequest
    form_class = DownloadResourceForm
    http_method_names = ["post"]

    # def form_valid(self, form):
    #     mail.send_mail(
    #         "Your resource",
    #         'Test'
    #         "from@example.com",
    #         [form.cleaned_data.get('email')],
    #         fail_silently=False,
        # )

    # email_template = get_template("resources/emails/send_unique_codes.txt")
    # url_prefix = '/'
    # unique_codes = '1'
    # mail.send_mail(
    #     f"{settings.SALES_EMAIL_PREFIX} Thankyou for buying our resource!",
    #     email_template.render(
    #         {
    #             "email_top": webhook_settings.email_top,
    #             "email_foot": webhook_settings.email_foot,
    #             "url_prefix": url_prefix,
    #             "unique_codes": unique_codes,
    #         }
    #     ),
    #     settings.SALES_FROM_EMAIL,
    #     ["james@goodbear.co.uk"],
    # )

    def get_success_url(self):

        from mailinglists.forms import SubscribeFormInpage

        url_prefix = f"https://www.restored-uk.org"
        email_template = get_template("resources/emails/send_resource_download.txt")

        resource_settings = ResourceSettings.for_site(self.request.site)

        mail.send_mail(
            f"{settings.SALES_EMAIL_PREFIX} Thankyou for downloading our resource!",
            email_template.render(
                {
                    "email_top": resource_settings.email_top,
                    "email_foot": resource_settings.email_foot,
                    "url_prefix": url_prefix,
                    "file": self.object.file.url,
                }
            ),
            settings.SALES_FROM_EMAIL,
            [self.object.email]
        )

        # if self.request.method == "POST":
        # if self.object.subscribe_me == True:
            
        # form = SubscribeFormInpage(self.request.POST)
        # # success_url = self.success_page.get_url()

        # if form.is_valid():
        #     form.subscribe_lists()
        #     # if success_url:
        #     #     return HttpResponseRedirect(self.success_page.get_url())
        #     # else:
        #     #     return HttpResponseRedirect('/subscribe/thank-you-subscribing/')

        if self.object.subscribe_me == True:
            mc = MailChimp(mc_user=settings.MAILCHIMP_USER_NAME, mc_api=settings.MAILCHIMP_KEY,)

            subscribe = {}
            subscribe['8642d2c006'] = True

            data = {
                "email_address": self.object.email,
                "status_if_new": "subscribed",
                "interests": subscribe,
                "merge_fields": {
                    "FNAME": self.object.first_name,
                    "LNAME": self.object.surname,
                },
            }

            mc.lists.members.create_or_update(
                list_id=settings.MAILCHIMP_LIST_ID, subscriber_hash=self.object.email, data=data,
            )

        return resource_settings.thankyou_page.url


def download_by_unique_code(request, code):
    uniquedownload = get_object_or_404(UniqueDownloadCode, code=code)
    UniqueDownloadCodeDownload.objects.create(unique_code=uniquedownload)
    return redirect(uniquedownload.file.url)


@csrf_exempt
@require_POST
def receive_webhook(request):
    webhook_settings = SquareWebHookSettings.for_site(request.site)

    # Check this is a valid request from Square:
    if not validate_webhook_signature(request, webhook_settings.signature_key):
        return HttpResponse("Invalid Signature", status=403)

    # Then process it:
    try:
        process_webhook(request, webhook_settings)
    except InvalidWebhookPayload as err:
        return HttpResponse("".join(err.args), status=405)

    return HttpResponse("Thanks, Square!")
