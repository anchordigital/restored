from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.db import models
from django.utils import timezone

from wagtail.admin.edit_handlers import FieldPanel, MultiFieldPanel, StreamFieldPanel
from wagtail.core.blocks import StreamBlock, BooleanBlock
from wagtail.core.fields import StreamField
from wagtail.core.models import Page
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.search import index

from .blocks.individual import ButtonBlock
from .blocks.streams import BaseStreamBlock, SideBarStreamBlock
from .querysets import ArchivablePageManager

from wagtailgmaps.edit_handlers import MapFieldPanel

from wagtailmenus.models import MenuPage
from wagtailmenus.panels import menupage_panel


class BasePage(MenuPage):
    share_description = models.TextField(
        blank=True,
        max_length=150,
        help_text="""
        Description/preview shown when this page is shared on social media.
        """,
    )
    share_image = models.ForeignKey(
        "wagtailimages.Image",
        blank=True,
        null=True,
        on_delete=models.PROTECT,
        related_name="+",
        help_text="""
        Image shown when this page is shared on social media.
        """,
    )

    menu_description = models.TextField(
        blank=True,
        max_length=150,
        help_text="""
        This will only display in a menu item that has less columns than items to show.
        """,
    )

    # Promote panels
    promote_panels = Page.promote_panels + [
        MultiFieldPanel(
            [FieldPanel("share_description"), ImageChooserPanel("share_image")],
            "Social meta",
            classname="page-options-panel",
        ),
        MultiFieldPanel(
            [FieldPanel("menu_description")],
            "Menu description",
            classname="menu-descriptor-panel",
        ),
        menupage_panel
    ]
   
    class Meta(Page.Meta):
        abstract = True

    

class ListPage(BasePage):
    """
        A generic / abstract listing page, lists child pages with pagination, and filtering.

        You must define:

            template = "whatever_list.html"
            filterset = <A django_filters.FilterSet>

            def get_queryset(self):
                returns a list of base pages to filter / show

    """

    def get_context(self, request):
        context = super().get_context(request)

        filter = self.filterset(request.GET, queryset=self.get_queryset())

        qs = filter.qs

        if "search" in request.GET:
            qs = qs.search(request.GET["search"])

        paginator = Paginator(qs, 250, orphans=5)
        page = request.GET.get("page")

        try:
            results = paginator.page(page)
        except PageNotAnInteger:
            results = paginator.page(1)
        except EmptyPage:
            results = paginator.page(paginator.num_pages)

        context["results"] = results
        context["paginator"] = paginator

        context["filter"] = filter

        return context

    class Meta:
        abstract = True


class ContentPage(BasePage):
    """
        This is the class all non-listing / non-homepage pages should inherit from.
        Includes the sidebar.

    """

    content = StreamField(BaseStreamBlock(required=False), blank=True)
    sidebar_content = StreamField(SideBarStreamBlock(required=False), blank=True)
    show_sidebar = models.BooleanField(default=True, blank=True)
    show_sidebar_auto_related = models.BooleanField(
        default=True,
        blank=True,
        help_text="Automatically show a list of related (by topic) pages in the sidebar.",
    )
    show_sidebar_metadata = models.BooleanField(
        default=False,
        blank=True,
        help_text="Should any categorisation / meta data (if any) be displayed in the sidebar?",
    )

    header_image = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
        help_text="Image displayed as a banner across the whole of the window width",
    )
    header_image_disable_grayscale = models.BooleanField(
        help_text="If the header image should always be shown in color",
        default=False,
        verbose_name="Disable Grayscale",
    )

    content_panels = Page.content_panels + [
        MultiFieldPanel(
            [ImageChooserPanel("header_image"), FieldPanel("header_image_disable_grayscale")],
            "Header image",
        ),
        StreamFieldPanel("content"),
        MultiFieldPanel(
            [
                FieldPanel("show_sidebar"),
                FieldPanel("show_sidebar_auto_related"),
                FieldPanel("show_sidebar_metadata"),
                StreamFieldPanel("sidebar_content"),
            ],
            "Sidebar",
        ),
        
    ]

    search_fields = BasePage.search_fields + [
        index.SearchField("content"),
        # probably not sidebar_content...
    ]

    class Meta(Page.Meta):
        abstract = True


class StandardPage(ContentPage):
    pass


class HomePage(BasePage):

    header_title = models.CharField(max_length=250)
    header_image = models.ForeignKey(
        "wagtailimages.Image", null=True, blank=True, on_delete=models.SET_NULL, related_name="+"
    )
    header_image_disable_grayscale = models.BooleanField(
        help_text="If the header image should always be shown in color",
        default=False,
        verbose_name="Disable Grayscale",
    )

    header_buttons = StreamField(
        StreamBlock([("button", ButtonBlock())], required=False), blank=True
    )

    hidden_from_public_search = models.BooleanField(
        help_text="This page, and subpages will not show up in public search results.",
        default=False,
        blank=True,
    )

    content = StreamField(BaseStreamBlock(), blank=True)

    content_panels = Page.content_panels + [
        MultiFieldPanel(
            [
                FieldPanel("header_title"),
                ImageChooserPanel("header_image"),
                FieldPanel("header_image_disable_grayscale"),
                StreamFieldPanel("header_buttons"),
            ],
            "Top Banner",
            classname="page-options-panel",
        ),
        StreamFieldPanel("content"),
    ]

    settings_panels = ContentPage.settings_panels + [FieldPanel("hidden_from_public_search")]


class ArchivablePage(ContentPage):
    """
        Archived pages should still be accessible by the direct URL.
        They should no longer be listed in normal searches / etc.
        They should display some kind of notice / text saying they're from the archive.
    """

    archived_after = models.DateTimeField(
        verbose_name="Archive date (will be unlisted, but still accessible by direct link)",
        blank=True,
        null=True,
        default=None,
        db_index=True,
    )

    settings_panels = ContentPage.settings_panels + [FieldPanel("archived_after")]

    objects = ArchivablePageManager()

    search_fields = ContentPage.search_fields + [index.FilterField("archived_after")]

    @property
    def status_string(self):
        if self.archived:
            return "Archived"
        return super().status_string

    @property
    def archived(self):
        return self.archived_after and self.archived_after < timezone.now()

    class Meta:
        abstract = True
