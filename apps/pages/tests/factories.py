import factory
import wagtail_factories
from wagtail.core.models import Site
from wagtail.tests.utils import WagtailPageTests

from pages.models import HomePage, StandardPage


class HomePageFactory(wagtail_factories.PageFactory):
    title = "Home Page"
    header_title = "A page for your home"

    parent = None

    class Meta:
        model = HomePage


class StandardPageFactory(wagtail_factories.PageFactory):
    title = factory.Faker("text")

    class Meta:
        model = StandardPage


class RestoredSiteTest(WagtailPageTests):
    """
        TestCase Base that sets up home page and other default pages ready for use...
    """

    def setUp(self):
        self.site = Site.objects.get()
        self.homepage = HomePageFactory.create(parent=None)
        self.site.root_page = self.homepage
        self.site.save()
