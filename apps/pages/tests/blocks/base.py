"""
    Helper stuff for tests.
"""
from django.test import SimpleTestCase, TestCase

import html5lib

PARSER = html5lib.HTMLParser(html5lib.treebuilders.getTreeBuilder("dom"), strict=True)


class InvalidHTML(Exception):
    pass


class TemplateTestMixin:
    def render(self, **kwargs):
        return self.block().render(kwargs)

    def assertValidHTML(self, value):
        """
        Doesn't check for valid attrs, etc, but at least catches formatting
        issues, quoting, etc.
        """
        exp = None
        try:
            PARSER.parseFragment(value)
        except Exception as _exp:
            exp = _exp
        if exp:
            raise InvalidHTML("\n---\n{}\n---\n{}".format(value, exp.args))

    def renderValid(self, **kwargs):
        text = self.render(**kwargs)
        self.assertValidHTML(text)
        return text


class SimpleTemplateCase(SimpleTestCase, TemplateTestMixin):
    pass


class TemplateCase(TestCase, TemplateTestMixin):
    pass
