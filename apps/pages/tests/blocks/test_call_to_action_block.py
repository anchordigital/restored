from unittest.mock import patch

from pages.blocks.individual import CallToActionBlock, LinkBlock

from .base import SimpleTemplateCase


class CallToActionBlockTest(SimpleTemplateCase):
    block = CallToActionBlock

    def test_blank(self):
        self.assertNotEqual("", self.renderValid())

    def test_description(self):
        output = self.renderValid(description="hello<b>WORLD!</b>")

        self.assertIn("hello<b>WORLD", output)

    def test_link_custom_url(self):
        link = LinkBlock().to_python({"link_to": "custom_url", "custom_url": "https://dev.ngo"})

        output = self.renderValid(link=link)

        self.assertIn("dev.ngo", output)

    @patch("wagtail.core.models.Page.objects.get")
    def test_link_text(self, mock):
        mock.return_value.url = "LOCAL_URL_MOCK"
        link = LinkBlock().to_python({"link_to": "page", "page": 0})

        text = self.renderValid(link=link, link_text="BANANAS!!!")

        self.assertIn("BANANAS!!!", text)

    @patch("wagtail.core.models.Page.objects.get")
    def test_link_page_url(self, mock):
        mock.return_value.url = "LOCAL_URL_MOCK"
        link = LinkBlock().to_python({"link_to": "page", "page": 0})

        text = self.renderValid(link=link)

        self.assertIn("LOCAL_URL_MOCK", text)

    @patch("wagtail.core.blocks.field_block.ChooserBlock.to_python")
    def test_link_file_url(self, mock):
        mock.return_value.url = "LOCAL_FILE_MOCK"
        link = LinkBlock().to_python({"link_to": "file", "file": 0})

        text = self.renderValid(link=link)

        self.assertIn("LOCAL_FILE_MOCK", text)
