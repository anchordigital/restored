from pages.blocks.individual import BoxLinkBlock, LinkBlock

from .base import SimpleTemplateCase


class ImageWithLinkBlockTest(SimpleTemplateCase):
    block = BoxLinkBlock

    def test_blank(self):
        self.assertNotEqual("", self.renderValid())

    def test_link(self):
        link = LinkBlock().to_python({"link_to": "custom_url", "custom_url": "https://dev.ngo"},)
        self.assertIn("https://dev.ngo", self.renderValid(link=link))

    def test_text(self):
        link = LinkBlock().to_python({"link_to": "custom_url", "custom_url": "https://dev.ngo"},)
        self.assertIn(
            "This link goes somewhere",
            self.renderValid(link=link, link_text="This link goes somewhere"),
        )
