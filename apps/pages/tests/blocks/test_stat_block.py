from pages.blocks.individual import StatBlock

from .base import SimpleTemplateCase


class ImageWithLinkBlockTest(SimpleTemplateCase):
    block = StatBlock

    def test_blank(self):
        self.assertNotEqual("", self.renderValid())

    def test_title(self):
        self.assertIn("TestTitle", self.renderValid(title="TestTitle"))

    def test_description(self):
        self.assertIn("DESCR!", self.renderValid(title="DESCR!"))
