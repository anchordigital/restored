from pages.blocks.individual import ImageBlock, LinkBlock

from .base import SimpleTemplateCase


class ImageBlockTest(SimpleTemplateCase):
    block = ImageBlock

    def test_blank(self):
        self.assertNotEqual("", self.renderValid())

    def test_link(self):
        link = LinkBlock().to_python({"link_to": "custom_url", "custom_url": "https://dev.ngo"},)
        self.assertIn("https://dev.ngo", self.renderValid(link=link))

    def test_caption(self):
        self.assertIn("A picture", self.renderValid(caption="A picture"))
