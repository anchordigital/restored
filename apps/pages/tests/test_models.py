from .factories import RestoredSiteTest, StandardPageFactory


class HomePageTest(RestoredSiteTest):
    """
        TestCase Base that sets up home page and other default pages ready for use...
    """

    def test_homepage(self):
        resp = self.client.get(self.homepage.get_url())
        self.assertContains(resp, self.homepage.title)

    def test_standardpage(self):
        about = StandardPageFactory.create(
            parent=self.homepage, title="About This demo site?", slug="about"
        )

        resp = self.client.get(about.get_url())
        self.assertContains(resp, about.title)
