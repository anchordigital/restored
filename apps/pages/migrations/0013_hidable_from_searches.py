# Generated by Django 2.2.12 on 2020-06-12 15:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0012_add_sidebar_heading'),
    ]

    operations = [
        migrations.AddField(
            model_name='homepage',
            name='hidden_from_public_search',
            field=models.BooleanField(blank=True, default=False, help_text='This page, and subpages will not show up in public search results.'),
        ),
    ]
