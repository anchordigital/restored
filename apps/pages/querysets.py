from django.utils import timezone

from wagtail.models import PageManager, PageQuerySet


class ArchivablePageQuerySet(PageQuerySet):
    def live(self):
        # NOTE: This only takes effect if you are querying the correct model type.
        #       So if you are doing a Page query (such as search), it will not take
        #       This into account, and you'll need to check the result .specific
        #       to get the correct 'real' model, and then check it's 'archived' property.
        return super().live().exclude(archived_after__lt=timezone.now())


ArchivablePageManager = PageManager.from_queryset(ArchivablePageQuerySet)
