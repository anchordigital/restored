"""
    Individual stand-alone blocks, complete elements.
"""
# Normally do not import URLBlock to use here, use the LinkBlock instead
from wagtail.core.blocks import (
    BooleanBlock,
    CharBlock,
    ChoiceBlock,
    ListBlock,
    RichTextBlock,
    StructBlock,
)
from wagtail.images.blocks import ImageChooserBlock
from wagtail.images.blocks import ImageChooserBlock


from .components import LinkBlock

class ImageBlock(StructBlock):
    image = ImageChooserBlock(required=True)
    description = CharBlock(required=False, help_text="Used as ALT text")
    caption = CharBlock(required=False, help_text="Shown below the image")
    link = LinkBlock(
        required=False, help_text="If you click on the image, should it open another page?"
    )
    disable_grayscale = BooleanBlock(
        help_text="If this picture should always be shown in color",
        default=False,
        blank=True,
        required=False,
    )

    class Meta:
        icon = "image"
        template = "blocks/image_block.html"


class HeadingBlock(StructBlock):
    heading_text = CharBlock(classname="title", required=True)
    size = ChoiceBlock(
        choices=[("", "Select a header size"), ("h2", "H2"), ("h3", "H3"), ("h4", "H4")],
        blank=True,
        required=False,
    )
    subheading_text = CharBlock(classname="subtitle", required=False)
    alignment = ChoiceBlock(
        choices=[("", "Select alignment"), ("left", "Left"), ("center", "Center")],
        blank=True,
        required=False,
    )

    class Meta:
        icon = "title"
        template = "blocks/heading_block.html"


class CallToActionBlock(StructBlock):
    title = CharBlock()
    description = RichTextBlock(features=["bold", "italic", "link"])
    link = LinkBlock(help_text="Where should the link lead to?")
    link_text = CharBlock(help_text="What should the text on the link say?")
    style = ChoiceBlock(
        choices=[("", "Select a button style"), ("button", "Button"), ("styled-link", "Styled link")],
        blank=True,
        required=True,
        default="button",
    )
    background = ChoiceBlock(
        choices=[("", "Select a background colour"), ("regular", "Regular"), ("faded", "Faded")],
        blank=True,
        required=True,
        default="regular",
    )
    width = ChoiceBlock(
        choices=[("", "Select a width"), ("regular", "Regular"), ("full", "Full")],
        blank=True,
        required=True,
        default="regular",
    )
    
    class Meta:
        icon = "link"
        template = "blocks/call_to_action_block.html"


class SummaryBlock(StructBlock):
    content = RichTextBlock(features=["bold", "italic", "link"])

    class Meta:
        icon = "title"
        template = "blocks/summary_block.html"


class ImageWithLinkBlock(StructBlock):
    background_image = ImageChooserBlock(required=False)
    disable_grayscale = BooleanBlock(
        help_text="If this picture should always be shown in color", default=False, required=False
    )
    link = LinkBlock(help_text="Where should the link lead to?")
    link_text = CharBlock(help_text="What should the text on the link say?")

    class Meta:
        icon = "link"
        template = "blocks/image_with_link_block.html"
        label = "Image link block"


class ImageMenuBlock(StructBlock):
    title = CharBlock(max_length=100)
    description = RichTextBlock(features=["bold", "italic", "link"])
    link = LinkBlock(help_text="Where should the link lead to?")
    link_text = CharBlock(help_text="What should the text on the link say?")
    images = ListBlock(ImageWithLinkBlock)

    class Meta:
        icon = "link"
        template = "blocks/image_menu_block.html"

class SliderBlock(StructBlock):

    heading_text = CharBlock(help_text="Add your title", required=False, blank=True)

    slides = ListBlock(
        StructBlock(
            [
                ("image", ImageChooserBlock(required=True)),
                ("title", CharBlock(required=True, max_length=40)),
                ("text", RichTextBlock(required=True)),
                ("link", LinkBlock(help_text="Where should the link lead to?")),
                ("link_text", CharBlock(help_text="What should the text on the link say?")),
            ]
        )
    )

    class Meta:  # noqa
        template = "blocks/slider_block.html"
        icon = "dots-horizontal"



class BoxLinkBlock(StructBlock):
    link_text = CharBlock()
    link = LinkBlock()

    class Meta:
        icon = "link"
        template = "blocks/box_link_block.html"


class ButtonBlock(StructBlock):
    link_text = CharBlock()
    link = LinkBlock()
    style = ChoiceBlock(
        choices=[("", "Select a button style"), ("button", "Button"), ("styled-link", "Styled link")],
        blank=True,
        required=True,
        default="button",
    )


    class Meta:
        icon = "link"
        template = "blocks/button_link_block.html"


class StatBlock(StructBlock):
    image = ImageChooserBlock(required=True)
    title = CharBlock(required=True)
    description = RichTextBlock(features=["bold", "italic", "link"])

    class Meta:
        icon = "link"
        template = "blocks/stat_block.html"


class QuoteBlock(StructBlock):
    quote = RichTextBlock(features=["bold", "italic"])
    citation = CharBlock(required=False)

    class Meta:
        icon = "link"
        template = "blocks/quote_block.html"


class SideBarHeading(StructBlock):
    title = CharBlock(required=True, max_length=50)

    class Meta:
        icon = "title"
        template = "blocks/sidebar_heading_block.html"

class MailingListSubscriptionBlock(StructBlock):
    title = CharBlock(required=True, max_length=50)
    description = RichTextBlock(features=["bold", "italic", "link"])
    form_class = "mailinglists.forms.SubscribeFormInpage"

    class Meta:
        icon = "fa-envelope"
        verbose_name = "mailing list subscription form"
        template = "mailinglists/blocks/subscription_form_inpage.html"

    def get_context(self, value, parent_context=None):
        from mailinglists.forms import SubscribeFormInpage

        context = super().get_context(value, parent_context)
        context.update(
            {"form": SubscribeFormInpage(),}
        )
        return context