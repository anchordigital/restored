"""
    StreamBlocks containing other blocks for use in pages.
"""
from wagtail.core.blocks import ListBlock, RawHTMLBlock, RichTextBlock, StreamBlock, StructBlock

from .individual import (
    BoxLinkBlock,
    ButtonBlock,
    CallToActionBlock,
    CharBlock,
    HeadingBlock,
    ImageBlock,
    ImageMenuBlock,
    SliderBlock,
    ImageWithLinkBlock,
    QuoteBlock,
    SideBarHeading,
    StatBlock,
    SummaryBlock,
    MailingListSubscriptionBlock
)


class SideBarStreamBlock(StreamBlock):
    heading = SideBarHeading()
    cta = CallToActionBlock()
    links = ListBlock(BoxLinkBlock())


class TwoColumnBlock(StructBlock):
    heading_text = CharBlock(classname="title", required=False)
    left_column = StreamBlock(
        [
            ("image", ImageBlock()),
            ("text", RichTextBlock()),
            ("cta", CallToActionBlock()),
            ("imagewithlink", ImageWithLinkBlock()),
            ("links", ListBlock(BoxLinkBlock())),
            ("buttons", ListBlock(ButtonBlock())),
        ],
        min_num=1,
    )
    right_column = StreamBlock(
        [
            ("image", ImageBlock()),
            ("text", RichTextBlock()),
            ("cta", CallToActionBlock()),
            ("imagewithlink", ImageWithLinkBlock()),
            ("links", ListBlock(BoxLinkBlock())),
            ("buttons", ListBlock(ButtonBlock())),
        ],
        min_num=1,
    )

    class Meta:
        icon = "fa-columns"
        form_template = "wagtailadmin/block_forms/two_column_block.html"
        template = "blocks/two_column_block.html"


class ThreeColumnBlock(StructBlock):
    heading_text = CharBlock(classname="title", required=False)
    column_one = StreamBlock(
        [
            ("image", ImageBlock()),
            ("heading", CharBlock(max_length=100)),
            ("text", RichTextBlock()),
            ("cta", CallToActionBlock()),
            ("imagewithlink", ImageWithLinkBlock()),
            ("links", ListBlock(BoxLinkBlock())),
            ("buttons", ListBlock(ButtonBlock())),
        ],
        min_num=1,
    )
    column_two = StreamBlock(
        [
            ("image", ImageBlock()),
            ("heading", CharBlock(max_length=100)),
            ("text", RichTextBlock()),
            ("cta", CallToActionBlock()),
            ("imagewithlink", ImageWithLinkBlock()),
            ("links", ListBlock(BoxLinkBlock())),
            ("buttons", ListBlock(ButtonBlock())),
        ],
        min_num=1,
    )
    column_three = StreamBlock(
        [
            ("image", ImageBlock()),
            ("heading", CharBlock(max_length=100)),
            ("text", RichTextBlock()),
            ("cta", CallToActionBlock()),
            ("imagewithlink", ImageWithLinkBlock()),
            ("links", ListBlock(BoxLinkBlock())),
            ("buttons", ListBlock(ButtonBlock())),
        ],
        min_num=1,
    )

    class Meta:
        icon = "fa-columns"
        form_template = "wagtailadmin/block_forms/two_column_block.html"
        template = "blocks/three_column_block.html"


class BaseStreamBlock(StreamBlock):
    heading = HeadingBlock()
    text = RichTextBlock(icon="fa-font")
    image = ImageBlock()
    html = RawHTMLBlock(label="HTML")
    two_column = TwoColumnBlock()
    three_column = ThreeColumnBlock()
    cta = CallToActionBlock()
    image_with_link = ImageWithLinkBlock()
    buttons = ListBlock(ButtonBlock())
    stats = ListBlock(StatBlock(), template="blocks/stat_list_block.html")
    image_menu = ImageMenuBlock()
    summary_block = SummaryBlock()
    quote_block = QuoteBlock()
    slider_block = SliderBlock()
    mailing_list_subscription_block = MailingListSubscriptionBlock()
