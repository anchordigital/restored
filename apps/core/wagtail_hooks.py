# from django.contrib.staticfiles.templatetags.staticfiles import static
from django.templatetags.static import static
from django.utils.html import format_html

from wagtail.core import hooks


@hooks.register("insert_global_admin_css")
def global_admin_css():
    return format_html('<link rel="stylesheet" href="{}">', static("dist/css/admin.css"))


@hooks.register("insert_global_admin_js")
def global_admin_js():
    return format_html(
        '<script src="{}"></script><script src="{}"></script>',
        static("dist/js/ie.js"),
        static("dist/js/admin.js"),
    )

