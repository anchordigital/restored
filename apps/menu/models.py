from django.db import models
from django.utils.translation import gettext_lazy as _
from modelcluster.fields import ParentalKey
from wagtail.images import get_image_model_string
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.admin.edit_handlers import FieldPanel, PageChooserPanel
from wagtailmenus.models import AbstractMainMenuItem
from wagtailmenus.models import AbstractLinkPage
from wagtail.core.blocks import ChoiceBlock

class CustomMainMenuItem(AbstractMainMenuItem):
    """A custom menu item model to be used by ``wagtailmenus.MainMenu``"""

    menu = ParentalKey(
        'wagtailmenus.MainMenu',
        on_delete=models.CASCADE,
        related_name="custom_menu_items", # important for step 3!
    )

    # megamenu = models.BooleanField(
    #     help_text="Is this a megamenu?",
    #     default=False,
    #     verbose_name="Megamneu",
    # )

    submenu_cols = models.CharField(
        choices=[("1", "1"), ("2", "2"), ("3", "3"),],
        help_text="Select how many colums this dropdown should have. Any extra items will be shown as blocks under the main nav items.",
        max_length=150,
        default=1
    )

    # Also override the panels attribute, so that the new fields appear
    # in the admin interface
    panels = (
        PageChooserPanel('link_page'),
        FieldPanel('link_url'),
        FieldPanel('url_append'),
        FieldPanel('link_text'),
        FieldPanel('allow_subnav'),
        FieldPanel('submenu_cols'),
    )

    @property
    def test(self):
        if self.link_page:
            return 'test'
        elif self.link_url:
            return 'test'
        return '#'



class LinkPage(AbstractLinkPage):
    pass
