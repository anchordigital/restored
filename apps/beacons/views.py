from django.contrib.contenttypes.models import ContentType
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.shortcuts import render
from django.views import View

from wagtail.core.models import Page
from wagtail.search.models import Query

from beacons.models import BeaconsPage
from news.models import NewsPage
from pages.models import HomePage, StandardPage
from resources.models import ResourcePage
import math
import logging
import django_filters


class BeaconsSearchFilter(django_filters.FilterSet):
    topic = django_filters.CharFilter(field_name="topic__slug")
    region = django_filters.CharFilter(field_name="location__slug")
    country = django_filters.CharFilter(field_name="location__slug")
    services = django_filters.CharFilter(field_name="services__slug")


class SearchView(View):
    allowed_methods = ["get"]

    def get_all_content_types(self):
        return [
            ContentType.objects.get_for_model(BeaconsPage),
            ContentType.objects.get_for_model(NewsPage),
            ContentType.objects.get_for_model(StandardPage),
            ContentType.objects.get_for_model(ResourcePage),
        ]

    def get_content_type_to_search(self, content_type_id):
        if content_type_id and type(content_type_id) == int:
            return ContentType.objects.filter(id=content_type_id).first()

    def geobounds(self, lat, lng):
        distance = 50
        radius = 3958.761

        self.maxLat = float(lat) + math.degrees(distance / radius)
        self.minLat = float(lat) - math.degrees(distance / radius)
       
        self.maxLng = float(lng) + math.degrees(distance / radius) / math.cos(math.degrees(float(lat)))
        self.minLng = float(lng) - math.degrees(distance / radius) / math.cos(math.degrees(float(lat)))

    def get_search_results(self, lat, lng, services):
        """
        Search for all public / not-hidden pages.

        Pages can be hidden by being under a HomePage (there can be multiple) which has
        an option set to hide it from public search.  For instance the Survivors' Area.
        """

        # hidden_homepages = HomePage.objects.filter(hidden_from_public_search=True)

        # if search_query:
        #     content_type_filter = self.get_content_type_to_search(content_type_id)

        #     pages = Page.objects.live().public()

        #     for hidden in hidden_homepages:
        #         pages = pages.not_descendant_of(hidden, inclusive=True)

        #     if content_type_filter:
        #         pages = pages.filter(content_type=content_type_filter)

        #     pages = pages.search(search_query)

        #     Query.get(search_query).add_hit()
        # else:
        #     pages = Page.objects.none()

        # distance = 50
        # radius = 3958.761

        # maxLat = float(lat) + math.degrees(distance / radius)
        # minLat = float(lat) - math.degrees(distance / radius)
       
        # maxLng = float(lng) + math.degrees(distance / radius) / math.cos(math.degrees(float(lat)))
        # minLng = float(lng) - math.degrees(distance / radius) / math.cos(math.degrees(float(lat)))


        # $orderby = " (POW((lng-"+lng+"),2) + POW((lat-"+$lat+"),2)) ASC";

        if lat and lng and services:
        	self.geobounds(lat, lng)
        	return BeaconsPage.objects.filter(lat__gt=self.minLat, lat__lt=self.maxLat, lng__gt=self.maxLng, lng__lt=self.minLng, services__slug=services).live().order_by("lat")
        elif lat and lng:
        	self.geobounds(lat, lng)
       		return BeaconsPage.objects.filter(lat__gt=self.minLat, lat__lt=self.maxLat, lng__gt=self.maxLng, lng__lt=self.minLng).live().order_by("lat")
       	elif services:
       		return BeaconsPage.objects.filter(services__slug=services).live().order_by("lat")
       	else:
       		return BeaconsPage.objects.filter().live().order_by("lat")
        # return BeaconsPage.objects.filter(services__slug="test").live().order_by("lat")
        #return list(pages)

    def get(self, *args, **kwargs):
        page = self.request.GET.get("page", 1)
        search_query = self.request.GET.get("q", "")
        lat = self.request.GET.get("lat", "")
        lng = self.request.GET.get("lng", "")
        services = self.request.GET.get("services", "")
        content_type_id = self.request.GET.get("content_type")

        search_results = self.get_search_results(lat, lng, services)

        filterset = BeaconsSearchFilter

        paginator = Paginator(search_results, 10)
        try:
            search_results = paginator.page(page)
        except PageNotAnInteger:
            search_results = paginator.page(1)
        except EmptyPage:
            search_results = paginator.page(paginator.num_pages)

        return render(
            self.request,
            "beacons/post_list.html",
            {
                "search_query": search_query,
                "results": search_results,
                "paginator": paginator,
                "title": "Beacons",
                "content_types": self.get_all_content_types(),
                "content_type_choice": content_type_id,
            },
        )


# from django.contrib.contenttypes.models import ContentType
# from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
# from django.shortcuts import render
# from django.views import View
# import django_filters
# from django.shortcuts import HttpResponse, get_object_or_404, redirect

# from wagtail.core.models import Page
# from wagtail.search.models import Query

# from beacons.models import BeaconsPage
# from pages.models import ArchivablePage, ListPage

# def members(request):

# 	def get_context(self, request):
#         context = super().get_context(request)
#         return context

# 	template = "beacons/post_list.html"
# 	return HttpResponse(template.render(context, request))
#     # return HttpResponse("Hello world!")

# class BeaconsPageFilter(django_filters.FilterSet):
#     topic = django_filters.CharFilter(field_name="topic__slug")
#     region = django_filters.CharFilter(field_name="location__slug")
#     country = django_filters.CharFilter(field_name="location__slug")
#     services = django_filters.CharFilter(field_name="services__slug")

# class BeaconsGeoSearch(View):
#     allowed_methods = ["get"]
#     template = "beacons/post_list.html"
#     subpage_types = ["BeaconsPage"]
#     filterset = BeaconsPageFilter
#     status_code = 200

#     # def get_context(self, request):
#     #     context = super().get_context(request)
#     #     return context

#     def get_queryset(self):
#         # return BeaconsPage.objects.descendant_of(self).live().order_by("-first_published_at")
#         # user_lat = $_GET['lat']
#         #user_lat = self.request.GET.get('lat')
#         return BeaconsPage.objects.descendant_of(self).filter(lat__gt=40, lat__lt=60.829580).live().order_by("-first_published_at")



