from django.utils import timezone

import factory
import wagtail_factories

from accounts.tests.factories import UserFactory
from Beacons.models import Category, BeaconsIndexPage, BeaconsPage


class CategoryFactory(factory.django.DjangoModelFactory):
    title = factory.Sequence(lambda n: f"Category {n}!")
    slug = factory.Sequence(lambda n: f"category_{n}!")

    class Meta:
        model = Category


class BeaconsIndexPageFactory(wagtail_factories.PageFactory):
    title = factory.Sequence(lambda n: f"Beacons List page {n}!")

    class Meta:
        model = BeaconsIndexPage


class BeaconsPageFactory(wagtail_factories.PageFactory):
    title = factory.Sequence(lambda n: f"Beacons Item {n}!")
    category = factory.SubFactory(CategoryFactory)
    author = factory.SubFactory(UserFactory)
    first_published_at = factory.LazyFunction(timezone.now)

    class Meta:
        model = BeaconsPage
