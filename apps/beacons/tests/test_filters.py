from django.test import TestCase

from categories.models import Topic
from Beacons.models import BeaconsPage, BeaconsPageFilter

from .factories import BeaconsPageFactory


class TestFilters(TestCase):
    def setUp(self):
        self.page = BeaconsPageFactory.create()

    def test_filters(self):
        f = BeaconsPageFilter({}, BeaconsPage.objects.all())

        self.assertEqual(f.qs.count(), 1)

    def test_filter_non_topic(self):
        self.assertEqual(self.page.topic.count(), 0)
        topic = Topic.objects.create(slug="thing")

        f = BeaconsPageFilter({"topic": topic.slug}, BeaconsPage.objects.all())
        self.assertEqual(f.qs.count(), 0)

    def test_filter_topic(self):
        self.assertEqual(self.page.topic.count(), 0)
        topic = Topic.objects.create(slug="thing")
        self.page.topic.add(topic)
        self.page.save()

        f = BeaconsPageFilter({"topic": topic.slug}, BeaconsPage.objects.all())
        self.assertEqual(f.qs.count(), 1)

        f = BeaconsPageFilter({}, BeaconsPage.objects.all())
        self.assertEqual(f.qs.count(), 1)
