from datetime import timedelta

from django.utils import timezone

import factory

from accounts.tests.factories import UserFactory
from pages.tests.factories import RestoredSiteTest

from .factories import CategoryFactory, BeaconsIndexPageFactory, BeaconsPageFactory


class TestBeaconsListPageTests(RestoredSiteTest):
    def setUp(self):
        super().setUp()
        self.listpage = BeaconsIndexPageFactory.create(parent=self.homepage)

    def test_single_result(self):
        Beaconspage = BeaconsPageFactory.create(parent=self.listpage)

        resp = self.client.get(self.listpage.get_url())
        self.assertContains(resp, Beaconspage.title)
        self.assertContains(resp, Beaconspage.get_url())

    def test_pagination_results(self):
        Beaconspage = BeaconsPageFactory.create(parent=self.listpage)
        category = CategoryFactory.create()
        user = UserFactory.create()
        factory.create_batch(
            BeaconsPageFactory, 100, parent=self.listpage, category=category, author=user
        )
        latest_Beaconspage = BeaconsPageFactory.create(parent=self.listpage)

        resp = self.client.get(self.listpage.get_url())

        self.assertNotContains(resp, Beaconspage.title)
        self.assertNotContains(resp, Beaconspage.get_url())

        self.assertContains(resp, latest_Beaconspage.title)
        self.assertContains(resp, latest_Beaconspage.get_url())

        resp = self.client.get(self.listpage.get_url() + "?page=10")

        self.assertContains(resp, Beaconspage.title)
        self.assertContains(resp, Beaconspage.get_url())

    def test_not_yet_archived_posts_shown_in_list(self):
        Beaconspage = BeaconsPageFactory.create(
            parent=self.listpage, archived_after=timezone.now() + timedelta(days=1)
        )
        resp = self.client.get(self.listpage.get_url())

        self.assertContains(resp, Beaconspage.title)
        self.assertContains(resp, Beaconspage.get_url())

    def test_archived_posts_not_shown_in_list(self):
        Beaconspage = BeaconsPageFactory.create(
            parent=self.listpage, archived_after=timezone.now() - timedelta(days=1)
        )

        resp = self.client.get(self.listpage.get_url())

        self.assertNotContains(resp, Beaconspage.title)
        self.assertNotContains(resp, Beaconspage.get_url())

    def test_unpublished_posts_not_shown_in_list(self):
        Beaconspage = BeaconsPageFactory.create(parent=self.listpage, live=False)

        resp = self.client.get(self.listpage.get_url())
        # Does not have unpublished Beacons:
        self.assertNotContains(resp, Beaconspage.title)
        self.assertNotContains(resp, Beaconspage.get_url())
