from django.db import models
from django.utils.functional import cached_property

import django_filters
from modelcluster.fields import ParentalManyToManyField
from wagtail.admin.edit_handlers import FieldPanel, MultiFieldPanel
# from wagtail.images.edit_handlers import ImageChooserPanel
from wagtailgeowidget import geocoders
from wagtailgeowidget.panels import GeoAddressPanel, GoogleMapsPanel
from wagtailgeowidget.helpers import geosgeometry_str_to_struct
from wagtail.images.edit_handlers import ImageChooserPanel

from pages.models import ArchivablePage, ListPage

from wagtailgmaps.edit_handlers import MapFieldPanel

import geopy
# from geopy.distance import VincentyDistance
# from geopy.distance import vincenty
from geopy.geocoders import get_geocoder_for_service
from geopy.exc import GeocoderUnavailable

GEOCODING_SERVICE = 'google'
GOOGLE_MAPS_KEY = 'AIzaSyDBXAeRRgYQYCwg_1J3sX69xukcFG13uE4'

class Location(models.Model):
    class CouldNotGeocode(Exception):
        pass

    SEARCH_RADIUS = 10
    COMPASS_BEARING = {
        'NORTH': 0,
        'EAST': 90,
        'SOUTH': 180,
        'WEST': 270,
    }

    name = models.CharField(max_length=255,
                            blank=True)
    latitude = models.FloatField()
    longitude = models.FloatField()
 
    def __str__(self):
        return self.name

    # def translate_point(self, bearing):
    #     origin = geopy.Point(self.latitude, self.longitude)
    #     destination = VincentyDistance(
    #         kilometers=self.SEARCH_RADIUS
    #     ).destination(origin, bearing)
    #     return destination

    def make_bounding_box(self, radius):
        max_lat = self.translate_point(
            self.COMPASS_BEARING['NORTH']
        ).latitude
        min_lat = self.translate_point(
            self.COMPASS_BEARING['SOUTH']
        ).latitude
        max_long = self.translate_point(
            self.COMPASS_BEARING['EAST']
        ).longitude
        min_long = self.translate_point(
            self.COMPASS_BEARING['WEST']
        ).longitude
        # Fetch locations within the bounding box
        locations = Location.objects.filter(
            latitude__gte=min_lat,
            latitude__lte=max_lat,
            longitude__gte=min_long,
            longitude__lte=max_long
        )
        return locations

    
    # def find_nearby_locations(self):
    #     bb_locations = self.make_bounding_box(self.SEARCH_RADIUS)
    #     return self.vincenty_filter(bb_locations, self.SEARCH_RADIUS)

    def geocode(self):
        geocoder_for_service = get_geocoder_for_service(GEOCODING_SERVICE)
        geocoder = geocoder_for_service(api_key=GOOGLE_MAPS_KEY)
        geocoded_location = geocoder.geocode(self.name)
        if geocoded_location:
            self.latitude = geocoded_location.latitude
            self.longitude = geocoded_location.longitude
        else:
            raise self.CouldNotGeocode("Location: "+self.name)

    def save(self, *args, **kwargs):
        try:
            self.geocode()
            super(Location, self).save(*args, **kwargs)
        except (GeocoderUnavailable, self.CouldNotGeocode) as e:
            pass


class BeaconsPage(ArchivablePage):

    # show_sidebar = false

    # image = models.ForeignKey(
    #     "wagtailimages.Image",
    #     null=True,
    #     blank=True,
    #     on_delete=models.SET_NULL,
    #     related_name="+",
    #     help_text="An image to be displayed at the top of the post",
    # )
    # image_disable_grayscale = models.BooleanField(
    #     help_text="If this picture should always be shown in color",
    #     default=False,
    #     verbose_name="Disable Grayscale",
    # )
    # formatted_address = models.CharField(max_length=255, default='')
    # latlng_address = models.CharField(max_length=255, default='')

    address = models.CharField(max_length=250, blank=True, null=True)
    location = models.CharField(max_length=250, blank=True, null=True)

    contact_name = models.CharField(max_length=250, blank=True, null=True)
    contact_email = models.CharField(max_length=250, blank=True, null=True)
    contact_phone = models.CharField(max_length=250, blank=True, null=True)
    contact_image = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
    )

    lat = models.FloatField(blank=True, null=True)
    lng = models.FloatField(blank=True, null=True)

    website = models.URLField(
        blank=True,
        verbose_name="Beacon website URL"
    )

    services = ParentalManyToManyField("categories.BeaconServices", blank=True)
    template = "beacons/post_detail.html"
    parent_page_types = ["beacons.BeaconsIndexPage"]

    # location_name = models.CharField(
    #     max_length=255,
    #     blank=True)
    # location = models.ForeignKey(
    #     'beacons.Location',
    #     null=True,
    #     blank=True,
    #     on_delete=models.SET_NULL,
    #     related_name='+',
    #     editable=False
    # )

    def geocode(self):
        geocoder_for_service = get_geocoder_for_service(GEOCODING_SERVICE)
        geocoder = geocoder_for_service(api_key=GOOGLE_MAPS_KEY)
        geocoded_location = geocoder.geocode(self.address)
        if geocoded_location:
            self.lat = geocoded_location.latitude
            self.lng = geocoded_location.longitude
        else:
            raise self.CouldNotGeocode("Location: "+self.name)

    def save(self, *args, **kwargs):
        # self.lat = 1
        # self.lng = 2
        self.geocode()
        super(BeaconsPage, self).save(*args, **kwargs)
        

    # def save_location(self):
    #     if self.location_name:
    #         self.location = Location.objects.get_or_create(
    #             # iexact doesn't work with get_or_create :(
    #             name=self.location_name.upper()
    #         )[0]
    #         self.location.save()
    #     if not self.location_name and self.location:
    #         # If self.location_name has been set to None,
    #         # make sure to set self.location to None
    #         self.location = None


    content_panels = ArchivablePage.content_panels + [
        FieldPanel("website"),
        MultiFieldPanel(
            [FieldPanel("services")], "Services"
        ),
        MultiFieldPanel([
            GeoAddressPanel("address", geocoder=geocoders.GOOGLE_MAPS),
            GoogleMapsPanel('location', address_field='address'),
        ], "Geo details"),
         MultiFieldPanel([
            FieldPanel("contact_name"),
            FieldPanel("contact_email"),
            FieldPanel("contact_phone"),
            ImageChooserPanel("contact_image"),
        ], "Church contact information"),
        # MapFieldPanel('formatted_address'),
        # MapFieldPanel('latlng_address', latlng=True),
        # FieldPanel('location_name'),
        # FieldPanel('lat'),
        # FieldPanel('lng')
    ]

    # @cached_property
    # def point(self):
    #     return geosgeometry_str_to_struct(self.location)

    # @property
    # def lat(self):
    #     return self.point['y']

    # @property
    # def lng(self):
    #     return self.point['x']

    @cached_property
    def regions(self):
        # This must be done here, rather than as a QuerySet method, as Page Preview
        # uses FakeQuerySet.
        return self.location.filter(location_type="R")

    @cached_property
    def countries(self):
        # This must be done here, rather than as a QuerySet method, as Page Preview
        # uses FakeQuerySet.
        return self.location.filter(location_type="C")

    def related_by_service(self):
        return (
            BeaconsPage.objects.live()
            .public()
            .exclude(pk=self.pk)
            .filter(beaconservices__in=self.beaconservices.all())
            .order_by("pk")
            .annotate(score=models.Count("pk"))
            .order_by("-score", "-first_published_at")[:5]
        )

    def __str__(self):
        return self.title


class BeaconsPageFilter(django_filters.FilterSet):
    topic = django_filters.CharFilter(field_name="topic__slug")
    region = django_filters.CharFilter(field_name="location__slug")
    country = django_filters.CharFilter(field_name="location__slug")
    services = django_filters.CharFilter(field_name="services__slug")


class BeaconsIndexPage(ListPage):
    allowed_methods = ["get"]
    template = "beacons/post_list.html"
    subpage_types = ["BeaconsPage"]
    filterset = BeaconsPageFilter

    def get_context(self, request):
        context = super().get_context(request)
        return context

    def get_queryset(self):
        # return BeaconsPage.objects.descendant_of(self).live().order_by("-first_published_at")
        # user_lat = $_GET['lat']
        #user_lat = self.request.GET.get('lat')
        return BeaconsPage.objects.descendant_of(self).filter().live().order_by("-first_published_at")
