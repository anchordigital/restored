from django.contrib.contenttypes.models import ContentType
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.shortcuts import render
from django.views import View

from wagtail.core.models import Page
from wagtail.search.models import Query

from news.models import NewsPage
from pages.models import HomePage, StandardPage
from resources.models import ResourcePage


class SearchView(View):
    allowed_methods = ["get"]

    def get_all_content_types(self):
        return [
            ContentType.objects.get_for_model(NewsPage),
            ContentType.objects.get_for_model(StandardPage),
            ContentType.objects.get_for_model(ResourcePage),
        ]

    def get_content_type_to_search(self, content_type_id):
        if content_type_id and type(content_type_id) == int:
            return ContentType.objects.filter(id=content_type_id).first()

    def get_search_results(self, search_query, content_type_id):
        """
        Search for all public / not-hidden pages.

        Pages can be hidden by being under a HomePage (there can be multiple) which has
        an option set to hide it from public search.  For instance the Survivors' Area.
        """

        hidden_homepages = HomePage.objects.filter(hidden_from_public_search=True)

        if search_query:
            content_type_filter = self.get_content_type_to_search(content_type_id)

            pages = Page.objects.live().public()

            for hidden in hidden_homepages:
                pages = pages.not_descendant_of(hidden, inclusive=True)

            if content_type_filter:
                pages = pages.filter(content_type=content_type_filter)

            pages = pages.search(search_query)

            Query.get(search_query).add_hit()
        else:
            pages = Page.objects.none()

        return list(pages)

    def get(self, *args, **kwargs):
        page = self.request.GET.get("page", 1)
        search_query = self.request.GET.get("q", "")
        content_type_id = self.request.GET.get("content_type")

        search_results = self.get_search_results(search_query, content_type_id)

        paginator = Paginator(search_results, 10)
        try:
            search_results = paginator.page(page)
        except PageNotAnInteger:
            search_results = paginator.page(1)
        except EmptyPage:
            search_results = paginator.page(paginator.num_pages)

        return render(
            self.request,
            "search/search_results.html",
            {
                "search_query": search_query,
                "search_results": search_results,
                "paginator": paginator,
                "content_types": self.get_all_content_types(),
                "content_type_choice": content_type_id,
            },
        )
