from django.contrib.auth.urls import urlpatterns as auth_urlpatterns
from django.urls import path

from . import views

urlpatterns = [
    path("search/", views.SearchView.as_view(), name="search"),
]

urlpatterns += auth_urlpatterns
