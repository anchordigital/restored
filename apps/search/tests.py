import json

from wagtail.core.models import Page

from news.tests.factories import NewsIndexPageFactory, NewsPageFactory
from pages.tests.factories import RestoredSiteTest, StandardPageFactory


class TestSearchLogic(RestoredSiteTest):
    def setUp(self):
        super().setUp()
        self.standardPage = StandardPageFactory.create(title="tomatoes", parent=self.homepage)

    def test_search_titles(self):
        self.assertEqual(Page.objects.live().search("tomatoes").count(), 1)

    def test_search_content(self):
        self.standardPage.content = json.dumps(
            [{"type": "text", "value": "All small hippos go to ballet class"}]
        )
        self.standardPage.save()

        StandardPageFactory.create(
            parent=self.homepage,
            content=json.dumps([{"type": "text", "value": "Hippos are kinda cool. And scary"}]),
        )

        self.assertEqual(Page.objects.live().search("ballet").count(), 1)
        self.assertEqual(Page.objects.live().search("hippos").count(), 2)

    def test_search_includes_newspages(self):
        newsindex = NewsIndexPageFactory.create(parent=self.homepage)
        NewsPageFactory.create(parent=newsindex, title="escaping asparagus")

        self.assertEqual(Page.objects.live().search("asparagus").count(), 1)
