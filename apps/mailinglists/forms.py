from django import forms

from crispy_forms.helper import FormHelper
from crispy_forms.layout import HTML, Field, Layout
from contact_forms.models import CaptchaFormBuilder
from wagtailcaptcha.models import WagtailCaptchaEmailForm
from captcha.fields import ReCaptchaField
from captcha.widgets import ReCaptchaV3

from . import lib
from .models import MailingList


class SubscribeForm(forms.Form):
    title = forms.CharField(label="Title (eg. Mr, Mrs, Miss)")
    first_name = forms.CharField(label="First name or initial")
    surname = forms.CharField()
    email = forms.EmailField()
    mailing_lists = forms.ModelMultipleChoiceField(
        queryset=MailingList.objects.all(),
        widget=forms.CheckboxSelectMultiple,
        help_text="A welcome email will be sent to you, make sure you check your junk mail.",
    )
    how_did_you_hear = forms.CharField(label="How did you hear about Restored?")
    error_css_class = "error"
    required_css_class = "required"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Field("title"),
            Field("first_name"),
            Field("surname"),
            Field("email"),
            Field("how_did_you_hear"),
            Field("mailing_lists"),
            HTML('<div class="button-container"><button class="submit">Send</button></div>'),
        )

    def subscribe_lists(self):
        lib.subscribe(self)

class SubscribeFormInpage(forms.Form):
    first_name = forms.CharField(label="First name or initial")
    surname = forms.CharField()
    email = forms.EmailField()
    recaptcha = ReCaptchaField(label="", widget=ReCaptchaV3())
    error_css_class = "error"
    required_css_class = "required"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Field("first_name", placeholder="First Name"),
            Field("surname", placeholder="Surname"),
            Field("email", placeholder="Email"),
            Field("recaptcha"),
            HTML('<div class="button-container"><button class="submit">Subscribe</button></div>'),
        )
        self.helper.form_action = '/subscribe/'

    def subscribe_lists(self):
        lib.subscribe(self)
