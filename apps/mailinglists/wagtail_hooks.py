from wagtail.contrib.modeladmin.options import ModelAdmin, ModelAdminGroup, modeladmin_register

from .models import MailingList, ResourceMailingList


class MailingListAdmin(ModelAdmin):
    model = MailingList
    list_display = ("title",)


class ResourceMailingListAdmin(ModelAdmin):
    model = ResourceMailingList
    list_display = ("title",)


class MailingListAdminGroup(ModelAdminGroup):
    menu_label = "Mailing lists"
    menu_icon = "folder-open-inverse"
    menu_order = 300
    items = (MailingListAdmin, ResourceMailingListAdmin)


modeladmin_register(MailingListAdminGroup)
