from django.db import models
from django.http import HttpResponseRedirect

from wagtail.admin.edit_handlers import PageChooserPanel, StreamFieldPanel
from wagtail.core.fields import StreamField
from wagtail.core.models import Orderable, Page

from pages.models import BasePage

from .blocks import SubscriptionStreamBlock


class MailingList(Orderable):
    title = models.CharField(max_length=100, db_index=True)

    def __str__(self):
        return self.title


class ResourceMailingList(Orderable):
    title = models.CharField(max_length=100, db_index=True)

    def __str__(self):
        return self.title


class SubscribePage(BasePage):
    """
    I was thinking about using AbstractForm for this, but I wanted
    to preserve the functionality of having the form in a block so editors
    can move it around within the streamfield.
    """

    content = StreamField(SubscriptionStreamBlock(), blank=True)
    success_page = models.ForeignKey(
        "wagtailcore.Page",
        null=True,
        on_delete=models.SET_NULL,
        related_name="+",
        help_text="Select the page to send the user to when they successfully submit this form",
    )

    content_panels = Page.content_panels + [
        StreamFieldPanel("content"),
        PageChooserPanel("success_page"),
    ]

    def serve(self, request, *args, **kwargs):
        from .forms import SubscribeFormInpage

        if request.method == "POST":
            
            form = SubscribeFormInpage(request.POST)
            success_url = self.success_page.get_url()

            if form.is_valid():
                form.subscribe_lists()
                if success_url:
                    return HttpResponseRedirect(self.success_page.get_url())
                else:
                    return HttpResponseRedirect('/subscribe/thank-you-subscribing/')

        return super().serve(request, *args, **kwargs)
