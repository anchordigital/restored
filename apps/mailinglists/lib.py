from django.conf import settings

from mailchimp3 import MailChimp
from django.core import mail

def subscribe(form):

    mail.send_mail(
        "Subject here",
        form.cleaned_data["email"].strip(),
        "from@example.com",
        ["james@goodbear.co.uk"],
        fail_silently=False,
    )

    #If mailing lists field does not exist then subscribe to general newsletter
    if "mailing_lists" in form:
        mailing_lists = form.cleaned_data["mailing_lists"]
        # If there are no mailinglist to subscribe to, then don't
        if not mailing_lists:
            return
    else:
        subscribe = {}
        subscribe['8642d2c006'] = True
    
    

    # For a mailinglist we'll need an email address
    email = form.cleaned_data["email"].strip()
    if email == "":
        return

    mc = MailChimp(mc_user=settings.MAILCHIMP_USER_NAME, mc_api=settings.MAILCHIMP_KEY,)

    if not subscribe:
        # First make sure we have the interest category id.
        interest_categories = mc.lists.interest_categories.all(
            settings.MAILCHIMP_LIST_ID, fields="categories.id,categories.title",
        )["categories"]

        interest_categories = {entry["title"]: entry["id"] for entry in interest_categories}
        interest_category_id = interest_categories.get(settings.MAILCHIMP_GROUP_NAME, None)
        if interest_category_id is None:
            text = "Mailchimp interest category name '{}' not found in {}."
            raise ValueError(text.format(settings.MAILCHIMP_GROUP_NAME, interest_categories),)

        # Build up a lookup table of id and the specific interests.
        interests = mc.lists.interest_categories.interests.all(
            settings.MAILCHIMP_LIST_ID, interest_category_id, fields="interests.id,interests.name",
        )["interests"]
        interests = {entry["name"]: entry["id"] for entry in interests}

        subscribe = {}

        text = "Mailchimp interest category '{}' does not have option '{}' in {}."
        for group in mailing_lists:
            group = str(group)
            gid = interests.get(group, None)

            if gid is None:
                raise ValueError(text.format(settings.MAILCHIMP_GROUP_NAME, group, interests,))
            subscribe[gid] = True

    # Build up the data from to send it.
    data = {
        "email_address": email,
        "status_if_new": "subscribed",
        "interests": subscribe,
        "merge_fields": {
            "TITLE": form.cleaned_data.get("title", ""),
            "FNAME": form.cleaned_data.get("first_name", ""),
            "LNAME": form.cleaned_data.get("surname", ""),
            "MMERGE3": form.cleaned_data.get("how_did_you_hear", ""),
        },
    }

    mc.lists.members.create_or_update(
        list_id=settings.MAILCHIMP_LIST_ID, subscriber_hash=email, data=data,
    )
