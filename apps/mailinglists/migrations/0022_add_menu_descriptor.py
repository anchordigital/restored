# Generated by Django 3.2 on 2023-06-01 07:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mailinglists', '0021_move_menupage_panel'),
    ]

    operations = [
        migrations.AddField(
            model_name='subscribepage',
            name='menu_descriptor',
            field=models.TextField(blank=True, help_text='\n        This will only display in a menu item that has less columns than items to show.\n        ', max_length=150),
        ),
    ]
