from wagtail.core.blocks import StructBlock

from pages.blocks import BaseStreamBlock


class MailingListSubscriptionBlock(StructBlock):
    form_class = "mailinglists.forms.SubscribeForm"

    class Meta:
        verbose_name = "mailing list subscription form"
        template = "mailinglists/blocks/subscription_form.html"

    def get_context(self, value, parent_context=None):
        from .forms import SubscribeForm

        context = super().get_context(value, parent_context)
        context.update(
            {"form": SubscribeForm(),}
        )
        return context


class SubscriptionStreamBlock(BaseStreamBlock):
    subscription_form = MailingListSubscriptionBlock()
