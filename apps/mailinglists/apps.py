from django.apps import AppConfig


class MailingListsConfig(AppConfig):
    name = "mailinglists"
    label = "mailinglists"
    verbose_name = "Mailing lists"
