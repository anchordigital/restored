from wagtail.contrib.modeladmin.options import ModelAdmin, ModelAdminGroup, modeladmin_register

from .models import Location, Topic, BeaconServices


class LocationAdmin(ModelAdmin):
    model = Location
    list_display = ("title", "slug", "location_type")
    list_filter = ("location_type",)


class TopicAdmin(ModelAdmin):
    model = Topic
    list_display = (
        "title",
        "slug",
    )

class BeaconServicesAdmin(ModelAdmin):
    model = BeaconServices
    list_display = (
        "title",
        "slug",
    )


class CategoriesGroup(ModelAdminGroup):
    menu_label = "Categories"
    menu_icon = "folder-open-inverse"
    menu_order = 200
    items = (TopicAdmin, LocationAdmin, BeaconServicesAdmin)


modeladmin_register(CategoriesGroup)
