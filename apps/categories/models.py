from django.db import models


class Location(models.Model):
    LOCATION_TYPE_CHOICES = (
        ("C", "Country"),
        ("R", "Region"),
    )

    location_type = models.CharField(max_length=1, choices=LOCATION_TYPE_CHOICES, db_index=True)
    title = models.CharField(max_length=100, db_index=True)
    slug = models.SlugField(max_length=100, unique=True)

    class Meta:
        ordering = ("location_type", "title")

    def __str__(self):
        return "%s: %s" % (self.get_location_type_display(), self.title)


class Topic(models.Model):
    title = models.CharField(max_length=100, db_index=True)
    slug = models.SlugField(max_length=100, unique=True)

    class Meta:
        ordering = ("title",)

    def __str__(self):
        return self.title

class BeaconServices(models.Model):
    title = models.CharField(max_length=100, db_index=True)
    slug = models.SlugField(max_length=100, unique=True)

    class Meta:
        ordering = ("title",)

    def __str__(self):
        return self.title
