from categories.models import Location, Topic, BeaconServices


def categories(request):
    return {"all_locations": Location.objects.all(), "all_topics": Topic.objects.all(), "all_beaconservices": BeaconServices.objects.all()}
