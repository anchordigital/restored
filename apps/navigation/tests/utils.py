from foliage.utils import build_page_tree
from wagtail.core.models import Page

# The standard test tree for navigation:
#
#   Root
#   └── Home Page
#       ├── Primary 1
#       ├── Primary 2
#       │   ├── Secondary 1
#       │   ├── Secondary 2
#       │   │   ├── Tertiary 1
#       │   │   └── Tertiary 2
#       │   ├── Secondary 3 (not live)
#       │   │   └── Hidden 1
#       │   ├── Secondary 4 (not in menus)
#       │   │   └── Hidden 2
#       │   └── Secondary 5
#       └── Primary 3
#
# - Home Page is a child of Root to match the Wagtail page structure.
# - Secondary 3 and Secondary 4 will not be displayed as neither are live pages - which also means
#   Hidden 1 and Hidden 2 will never be seen.


def build_test_tree():
    root_page = Page.objects.get(title="Root")

    return build_page_tree(
        tree=[
            (
                Page(title="Home Page", show_in_menus=True),
                [
                    Page(title="Primary 1", show_in_menus=True),
                    (
                        Page(title="Primary 2", show_in_menus=True),
                        [
                            Page(title="Secondary 1", show_in_menus=True),
                            (
                                Page(title="Secondary 2", show_in_menus=True),
                                [
                                    Page(title="Tertiary 1", show_in_menus=True),
                                    Page(title="Tertiary 2", show_in_menus=True),
                                ],
                            ),
                            (
                                Page(title="Secondary 3", show_in_menus=True, live=False),
                                [Page(title="Hidden 1", show_in_menus=True)],
                            ),
                            (
                                Page(title="Secondary 4"),
                                [Page(title="Hidden 2", show_in_menus=True)],
                            ),
                            Page(title="Secondary 5", show_in_menus=True),
                        ],
                    ),
                    Page(title="Primary 3", show_in_menus=True),
                ],
            )
        ],
        root=root_page,
    )
