from django.template import Context, Template
from django.test import RequestFactory, TestCase

from wagtail.core.models import Page, Site

from navigation.templatetags.level_navigation import get_pages_at_depth

from .utils import build_test_tree


class GetPagesAtDepthTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.page_tree = build_test_tree()
        cls.root_page = cls.page_tree[0]

    def test_primary(self):
        active_page = Page.objects.get(title="Primary 1")

        with self.assertNumQueries(1):
            page_list = get_pages_at_depth(root_page=self.root_page, page=active_page)

        self.assertEqual(
            page_list,
            [
                (Page.objects.get(title="Primary 1"), True),
                (Page.objects.get(title="Primary 2"), False),
                (Page.objects.get(title="Primary 3"), False),
            ],
        )

    def test_secondary(self):
        active_page = Page.objects.get(title="Secondary 2")

        with self.assertNumQueries(1):
            page_list = get_pages_at_depth(root_page=self.root_page, page=active_page, level=2)

        self.assertEqual(
            page_list,
            [
                (Page.objects.get(title="Secondary 1"), False),
                (Page.objects.get(title="Secondary 2"), True),
                (Page.objects.get(title="Secondary 5"), False),
            ],
        )

    def test_non_wagtail(self):
        with self.assertNumQueries(1):
            page_list = get_pages_at_depth(root_page=self.root_page, page=None)

        self.assertEqual(
            page_list,
            [
                (Page.objects.get(title="Primary 1"), False),
                (Page.objects.get(title="Primary 2"), False),
                (Page.objects.get(title="Primary 3"), False),
            ],
        )

    def test_navigation_too_deep(self):
        active_page = Page.objects.get(title="Secondary 1")

        with self.assertNumQueries(0):
            page_list = get_pages_at_depth(root_page=self.root_page, page=active_page, level=4)

        self.assertEqual(page_list, [])


class NavigationTagTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.page_tree = build_test_tree()
        cls.site = Site.objects.create(
            hostname="navigation.example.org", root_page=cls.page_tree[0]
        )
        cls.request = RequestFactory()
        cls.request.site = cls.site

        # Force site root pages to be cached
        Site.get_site_root_paths()

    def test_primary_navigation(self):
        context = Context({"request": self.request, "self": Page.objects.get(title="Tertiary 1")})
        template = Template(
            """
            {% load level_navigation %}
            {% primary_navigation self %}
            """
        )

        with self.assertNumQueries(1):
            html = template.render(context=context)

        self.assertHTMLEqual(
            html,
            """
              <ul class="primary">
                <li>
                  <a href="/primary-1/">Primary 1</a>
                </li>
                <li>
                  <a href="/primary-2/" class="active">Primary 2</a>
                </li>
                <li>
                  <a href="/primary-3/">Primary 3</a>
                </li>
              </ul>
            """,
        )
