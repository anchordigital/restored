from django.template import Context, Template
from django.test import RequestFactory, TestCase

from wagtail.core.models import Page, Site

from navigation.templatetags.tree_navigation import render_tree, skip_hidden_pages

from .utils import build_test_tree


class SkipHiddenPagesTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.page_tree = build_test_tree()
        cls.root_page = cls.page_tree[0]

    def test_something(self):
        # Force queryset evaluation to show no queries are being made later
        page_tree = Page.get_tree(parent=self.root_page)
        list(page_tree)

        with self.assertNumQueries(0):
            page_list = skip_hidden_pages(queryset=page_tree)

        self.assertEqual(
            page_list,
            [
                Page.objects.get(title="Home Page"),
                Page.objects.get(title="Primary 1"),
                Page.objects.get(title="Primary 2"),
                Page.objects.get(title="Secondary 1"),
                Page.objects.get(title="Secondary 2"),
                Page.objects.get(title="Tertiary 1"),
                Page.objects.get(title="Tertiary 2"),
                Page.objects.get(title="Secondary 5"),
                Page.objects.get(title="Primary 3"),
            ],
        )


class RenderTreeTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.page_tree = build_test_tree()
        cls.root_page = cls.page_tree[0]

    def test_show_all(self):
        primary_two = Page.objects.get(title="Primary 2")

        with self.assertNumQueries(1):
            page_tree = render_tree(
                root_path=primary_two.path, root_depth=primary_two.depth, show_all=True
            )

        self.assertEqual(
            page_tree,
            [
                (
                    Page.objects.get(title="Secondary 1"),
                    {"active": False, "close": [], "level": 0, "open": True},
                ),
                (
                    Page.objects.get(title="Secondary 2"),
                    {"active": False, "close": [], "level": 0, "open": False},
                ),
                (
                    Page.objects.get(title="Tertiary 1"),
                    {"active": False, "close": [], "level": 1, "open": True},
                ),
                (
                    Page.objects.get(title="Tertiary 2"),
                    {"active": False, "close": [0], "level": 1, "open": False},
                ),
                (
                    Page.objects.get(title="Secondary 5"),
                    {"active": False, "close": [0], "level": 0, "open": False},
                ),
            ],
        )

    def test_show_all_false(self):
        primary_two = Page.objects.get(title="Primary 2")

        with self.assertNumQueries(1):
            page_tree = render_tree(
                root_path=self.root_page.path, root_depth=self.root_page.depth, page=primary_two
            )

        self.assertEqual(
            page_tree,
            [
                (
                    Page.objects.get(title="Primary 1"),
                    {
                        "active": False,
                        "previous_active": False,
                        "close": [],
                        "level": 0,
                        "open": True,
                    },
                ),
                (
                    Page.objects.get(title="Primary 2"),
                    {
                        "active": True,
                        "previous_active": False,
                        "close": [],
                        "level": 0,
                        "open": False,
                    },
                ),
                (
                    Page.objects.get(title="Secondary 1"),
                    {
                        "active": False,
                        "previous_active": True,
                        "close": [],
                        "level": 1,
                        "open": True,
                    },
                ),
                (
                    Page.objects.get(title="Secondary 2"),
                    {
                        "active": False,
                        "previous_active": False,
                        "close": [],
                        "level": 1,
                        "open": False,
                    },
                ),
                (
                    Page.objects.get(title="Secondary 5"),
                    {
                        "active": False,
                        "previous_active": False,
                        "close": [0],
                        "level": 1,
                        "open": False,
                    },
                ),
                (
                    Page.objects.get(title="Primary 3"),
                    {
                        "active": False,
                        "previous_active": False,
                        "close": [0],
                        "level": 0,
                        "open": False,
                    },
                ),
            ],
        )

    def test_no_page_show_all_false(self):
        with self.assertNumQueries(1):
            page_tree = render_tree(root_path=self.root_page.path, root_depth=self.root_page.depth)

        self.assertEqual(
            page_tree,
            [
                (
                    Page.objects.get(title="Primary 1"),
                    {"active": False, "close": [], "level": 0, "open": True},
                ),
                (
                    Page.objects.get(title="Primary 2"),
                    {"active": False, "close": [], "level": 0, "open": False},
                ),
                (
                    Page.objects.get(title="Primary 3"),
                    {"active": False, "close": [0], "level": 0, "open": False},
                ),
            ],
        )


class NavigationTagTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.page_tree = build_test_tree()
        cls.site = Site.objects.create(
            hostname="navigation.example.org", root_page=cls.page_tree[0]
        )
        cls.request = RequestFactory()
        cls.request.site = cls.site

        # Force site root pages to be cached
        Site.get_site_root_paths()

    def test_subtree_not_shown(self):
        context = Context({"request": self.request, "self": Page.objects.get(title="Home Page")})
        template = Template(
            """
            {% load tree_navigation %}
            {% subtree_navigation self %}
            """
        )

        with self.assertNumQueries(0):
            html = template.render(context=context)

        self.assertHTMLEqual(html, "")
