from django import template
from django.db.models import Q

from wagtail.core.models import Page

register = template.Library()


def skip_hidden_pages(queryset):
    """
    Loop through all pages in a queryset, and hides any pages which aren't visible (and their
    entire subtree) from navigation.
    """
    skip_path = None
    page_list = []

    for page in queryset:
        # If a skip_path is set, then we continue skipping pages which are children of that page -
        # so any pages which start with the same path get excluded from navigation.
        if skip_path is not None:
            if page.path.startswith(skip_path):
                continue
            else:
                skip_path = None

        # If a page isn't live, or shown in menus - then skip it and the entire subtree
        if page.live is False or page.show_in_menus is False:
            skip_path = page.path
            continue

        page_list.append(page)

    return page_list


def render_tree(root_path, root_depth, page=None, max_depth=2, show_all=False):
    page_qs = Page.objects.all()

    # The maximum search depth - current level, plus 2 additional levels
    max_search_depth = root_depth + max_depth + 1

    if show_all:
        # Showing all items is a simple queryset - just show all pages of the root page which are
        # between the allowed depths.
        page_qs = page_qs.filter(
            path__startswith=root_path, depth__gt=root_depth, depth__lte=max_search_depth
        )
    else:
        # Always show immediate children of the root page
        page_q = Q(path__startswith=root_path, depth=root_depth + 1)

        if page:
            # For each level after immediate children, only show sibling pages. This will filter
            # out anything which is outside the visible tree.

            # Ensure that if a page is more than 2 levels in, we don't continue to show more
            max_level_depth = min([max_search_depth, page.depth + 1])

            for level_depth in range(root_depth + 1, max_level_depth):
                base_path = Page._get_basepath(page.path, level_depth)

                page_q = page_q | Q(path__startswith=base_path, depth=level_depth + 1)

        page_qs = page_qs.filter(page_q)

    page_qs = skip_hidden_pages(queryset=page_qs)
    page_tree = Page.get_annotated_list_qs(qs=page_qs)

    for page_index, (tree_page, tree_info) in enumerate(page_tree):
        if page:
            tree_info["active"] = page.path.startswith(tree_page.path)

            # Check whether the previous page was active - helpful in the template
            if page_index > 0:
                previous_tree_page = page_tree[page_index - 1][0]
                tree_info["previous_active"] = page.path.startswith(previous_tree_page.path)
            else:
                tree_info["previous_active"] = False
        else:
            tree_info["active"] = False

    return page_tree


@register.inclusion_tag("navigation/tree.html", takes_context=True)
def tree_navigation(context, page=None, max_depth=2, show_all=False, css_class="tree"):
    request = context["request"]
    root_page = request.site.root_page

    page_tree = render_tree(
        root_path=root_page.path,
        root_depth=root_page.depth,
        page=page,
        max_depth=max_depth,
        show_all=show_all,
    )

    return {"request": request, "page_tree": page_tree, "css_class": css_class}


@register.inclusion_tag("navigation/tree.html", takes_context=True)
def subtree_navigation(context, page, min_level=1, max_depth=2, show_all=False, css_class="tree"):
    request = context["request"]
    root_page = request.site.root_page

    # A sane default incase we can't render a tree
    page_tree = Page.objects.none()

    # page_depth is the "true" depth - how many levels below the root page of the site
    page_depth = page.depth - root_page.depth

    if page_depth >= min_level:
        # Use the current page, strip the path to a level higher (the parent page)
        subtree_root_depth = root_page.depth + min_level
        subtree_root_path = Page._get_basepath(page.path, subtree_root_depth)

        page_tree = render_tree(
            root_path=subtree_root_path,
            root_depth=subtree_root_depth,
            page=page,
            max_depth=max_depth,
            show_all=show_all,
        )

    return {"request": request, "page_tree": page_tree, "css_class": css_class}
