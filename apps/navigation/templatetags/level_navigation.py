from django import template

from wagtail.core.models import Page

register = template.Library()


def get_pages_at_depth(root_page, page, level=1):
    if not page:
        # If a page isn't available (a non-Wagtail view) - then swap to the root page. This allows
        # primary navigation to be seen from any view, without any other pages being shown as
        # active.
        page = root_page

    # page_depth is the "true" depth - how many levels below the root page of the site
    page_depth = page.depth - root_page.depth

    # Page isn't deep enough to show this level of navigation
    if level > page_depth + 1:
        return []

    # Use the current page, strip the path to a level higher (the parent page)
    search_path = Page._get_basepath(page.path, root_page.depth + level - 1)

    # Find all pages with the parent path, but one level below
    child_depth = root_page.depth + level
    page_qs = (
        Page.objects.filter(path__startswith=search_path, depth=child_depth)
        .live()
        .in_menu()
        .order_by("path")
    )

    # Build a list of tuples, which includes if the page is activate in navigation. If the page
    # has the same path as the one being requested - then it's a direct descendent and can be
    # considered active.
    page_list = []
    for nav_page in page_qs:
        page_list.append((nav_page, page.path.startswith(nav_page.path)))

    return page_list


@register.inclusion_tag("navigation/level.html", takes_context=True)
def navigation_at_level(context, page=None, level=1, css_class=None):
    """
    Render a list of pages at a specific navigation level.

    Level 1 pages are direct descendents of the root page (primary navigation), higher levels are
    further down the tree.

    This is an inclusion tag mostly used by other navigation tags (primary_navigation,
    secondary_navigation, tertiary_navigation) - however it exists here incase additional levels
    of navigation are needed.

    At this point it's recommended that you might want to use a page tree for navigation instead.
    """
    # Use the level number if this isn't a named navigation level
    if css_class is None:
        css_class = "level-{}".format(level)

    root_page = context["request"].site.root_page

    return {
        "request": context["request"],
        "page_list": get_pages_at_depth(root_page=root_page, page=page, level=level),
        "css_class": css_class,
    }


@register.inclusion_tag("navigation/level.html", takes_context=True)
def primary_navigation(context, page=None, css_class="primary"):
    """ Render a list of primary level pages. """
    return navigation_at_level(context=context, page=page, css_class=css_class)


@register.inclusion_tag("navigation/level.html", takes_context=True)
def secondary_navigation(context, page=None, css_class="secondary"):
    """ Render a list of secondary level pages. """
    return navigation_at_level(context=context, page=page, level=2, css_class=css_class)


@register.inclusion_tag("navigation/level.html", takes_context=True)
def tertiary_navigation(context, page=None, css_class="tertiary"):
    """ Render a list of tertiary level pages. """
    return navigation_at_level(context=context, page=page, level=3, css_class=css_class)
