from django.core.exceptions import ValidationError
from django.db import models
from django.shortcuts import Http404, redirect

from wagtail.admin.edit_handlers import FieldPanel, HelpPanel, PageChooserPanel
from wagtail.core.models import Page


class RedirectPage(Page):
    """
        Allow pages in multiple places in the nav-tree - use RedirectPages to point
        to the canonical URL.
    """

    redirect_to_page = models.ForeignKey(
        Page,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="page_redirects",
        verbose_name="Redirect-to-Page",
        help_text="Destination page to redirect to",
    )
    redirect_to_url = models.URLField(
        blank=True,
        verbose_name="Redirect-to-URL",
        help_text="Destination URL to redirect to (if no Destination page selected)",
    )

    content_panels = Page.content_panels + [
        HelpPanel(
            """ This page type will redirect visitors to another page or URL.
                If you want it to appear in the navigation menus, remember to tick that
                option in the Promote Panel. """
        ),
        PageChooserPanel("redirect_to_page"),
        FieldPanel("redirect_to_url"),
    ]

    def clean(self):
        super().clean()

        if self.redirect_to_url and self.redirect_to_page:
            raise ValidationError(
                {
                    "redirect_to_url": (
                        "You cannot specify a redirect-to-page"
                        " and a redirect-to-URL at the same time."
                    )
                }
            )

        if not self.redirect_to_url and not self.redirect_to_page:
            raise ValidationError(
                {
                    "redirect_to_page": (
                        "Please specify either a redirect-to-page or a redirect-to-URL."
                    )
                }
            )

    def serve(self, request, *args, **kwargs):
        if self.redirect_to_page:
            return redirect(self.redirect_to_page.get_url())
        elif self.redirect_to_url:
            return redirect(self.redirect_to_url)
        else:
            raise Http404("Redirect-Page not found")
