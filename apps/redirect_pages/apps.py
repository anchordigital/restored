from django.apps import AppConfig


class RedirectPagesConfig(AppConfig):
    name = "redirect_pages"
