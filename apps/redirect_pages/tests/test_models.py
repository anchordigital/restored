from pages.tests.factories import RestoredSiteTest

from .factories import RedirectPageFactory


class RedirectPageTest(RestoredSiteTest):
    def test_page_redirect(self):
        redirectpage = RedirectPageFactory.create(parent=self.homepage)

        other_page = redirectpage.redirect_to_page

        response = self.client.get(redirectpage.get_url())
        self.assertRedirects(response, other_page.get_url(), fetch_redirect_response=False)

    def test_url_redirect(self):
        redirectpage = RedirectPageFactory.create(
            parent=self.homepage, redirect_to_url="https://www.dev.ngo", redirect_to_page=None
        )

        response = self.client.get(redirectpage.get_url())
        self.assertRedirects(response, "https://www.dev.ngo", fetch_redirect_response=False)
