import factory
import wagtail_factories

from pages.tests.factories import StandardPageFactory
from redirect_pages.models import RedirectPage


class RedirectPageFactory(wagtail_factories.PageFactory):
    title = factory.Sequence(lambda n: f"RedirectPage {n}")
    redirect_to_page = factory.SubFactory(StandardPageFactory)

    class Meta:
        model = RedirectPage
