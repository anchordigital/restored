from wagtail.contrib.modeladmin.options import ModelAdmin, modeladmin_register

from .models import Category


class CategoryAdmin(ModelAdmin):
    model = Category
    menu_label = "News categories"
    list_display = ("title", "slug")


modeladmin_register(CategoryAdmin)
