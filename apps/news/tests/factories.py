from django.utils import timezone

import factory
import wagtail_factories

from accounts.tests.factories import UserFactory
from news.models import Category, NewsIndexPage, NewsPage


class CategoryFactory(factory.django.DjangoModelFactory):
    title = factory.Sequence(lambda n: f"Category {n}!")
    slug = factory.Sequence(lambda n: f"category_{n}!")

    class Meta:
        model = Category


class NewsIndexPageFactory(wagtail_factories.PageFactory):
    title = factory.Sequence(lambda n: f"News List page {n}!")

    class Meta:
        model = NewsIndexPage


class NewsPageFactory(wagtail_factories.PageFactory):
    title = factory.Sequence(lambda n: f"News Item {n}!")
    category = factory.SubFactory(CategoryFactory)
    author = factory.SubFactory(UserFactory)
    first_published_at = factory.LazyFunction(timezone.now)

    class Meta:
        model = NewsPage
