from django.test import TestCase

from categories.models import Topic
from news.models import NewsPage, NewsPageFilter

from .factories import NewsPageFactory


class TestFilters(TestCase):
    def setUp(self):
        self.page = NewsPageFactory.create()

    def test_filters(self):
        f = NewsPageFilter({}, NewsPage.objects.all())

        self.assertEqual(f.qs.count(), 1)

    def test_filter_non_topic(self):
        self.assertEqual(self.page.topic.count(), 0)
        topic = Topic.objects.create(slug="thing")

        f = NewsPageFilter({"topic": topic.slug}, NewsPage.objects.all())
        self.assertEqual(f.qs.count(), 0)

    def test_filter_topic(self):
        self.assertEqual(self.page.topic.count(), 0)
        topic = Topic.objects.create(slug="thing")
        self.page.topic.add(topic)
        self.page.save()

        f = NewsPageFilter({"topic": topic.slug}, NewsPage.objects.all())
        self.assertEqual(f.qs.count(), 1)

        f = NewsPageFilter({}, NewsPage.objects.all())
        self.assertEqual(f.qs.count(), 1)
