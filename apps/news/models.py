from django.db import models
from django.utils.functional import cached_property

import django_filters
from modelcluster.fields import ParentalManyToManyField
from wagtail.admin.edit_handlers import FieldPanel, MultiFieldPanel
from wagtail.images.edit_handlers import ImageChooserPanel

from pages.models import ArchivablePage, ListPage


class Category(models.Model):
    title = models.CharField(max_length=255, unique=True)
    slug = models.SlugField(max_length=100, unique=True)

    def __str__(self):
        return self.title


class NewsPage(ArchivablePage):
    category = models.ForeignKey(Category, null=True, on_delete=models.SET_NULL)
    image = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
        help_text="An image to be displayed at the top of the post",
    )
    image_disable_grayscale = models.BooleanField(
        help_text="If this picture should always be shown in color",
        default=False,
        verbose_name="Disable Grayscale",
    )

    author = models.ForeignKey("accounts.User", null=True, on_delete=models.SET_NULL, blank=True)
    location = ParentalManyToManyField("categories.Location", blank=True)
    topic = ParentalManyToManyField("categories.Topic", blank=True)

    template = "news/post_detail.html"
    parent_page_types = ["news.NewsIndexPage"]

    content_panels = ArchivablePage.content_panels + [
        MultiFieldPanel(
            [
                FieldPanel("author"),
                FieldPanel("category"),
                FieldPanel("location"),
                FieldPanel("topic"),
            ],
            "Categorisation",
        ),
        MultiFieldPanel(
            [ImageChooserPanel("image"), FieldPanel("image_disable_grayscale")], "Featured Image"
        ),
    ]

    @cached_property
    def regions(self):
        # This must be done here, rather than as a QuerySet method, as Page Preview
        # uses FakeQuerySet.
        return self.location.filter(location_type="R")

    @cached_property
    def countries(self):
        # This must be done here, rather than as a QuerySet method, as Page Preview
        # uses FakeQuerySet.
        return self.location.filter(location_type="C")

    def related_by_topic(self):
        return (
            NewsPage.objects.live()
            .public()
            .exclude(pk=self.pk)
            .filter(topic__in=self.topic.all())
            .order_by("pk")
            .annotate(score=models.Count("pk"))
            .order_by("-score", "-first_published_at")[:5]
        )

    def __str__(self):
        return self.title


class NewsPageFilter(django_filters.FilterSet):
    category = django_filters.CharFilter(field_name="category__slug")
    topic = django_filters.CharFilter(field_name="topic__slug")
    region = django_filters.CharFilter(field_name="location__slug")
    country = django_filters.CharFilter(field_name="location__slug")


class NewsIndexPage(ListPage):
    template = "news/post_list.html"
    subpage_types = ["NewsPage"]
    filterset = NewsPageFilter

    def get_context(self, request):
        context = super().get_context(request)
        context["news_categories"] = Category.objects.filter(
            newspage__in=self.get_queryset()
        ).distinct()
        return context

    def get_queryset(self):
        return NewsPage.objects.descendant_of(self).live().order_by("-first_published_at")
