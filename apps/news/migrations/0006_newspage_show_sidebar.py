# Generated by Django 2.2.12 on 2020-06-02 09:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0005_newspage_archived_after'),
    ]

    operations = [
        migrations.AddField(
            model_name='newspage',
            name='show_sidebar',
            field=models.BooleanField(blank=True, default=True),
        ),
    ]
