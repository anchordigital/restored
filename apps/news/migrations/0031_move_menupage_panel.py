# Generated by Django 3.2 on 2023-06-01 07:36

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0030_add_menu_button_option'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='newsindexpage',
            name='menu_button',
        ),
        migrations.RemoveField(
            model_name='newspage',
            name='menu_button',
        ),
    ]
