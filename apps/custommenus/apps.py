from django.apps import AppConfig


class CustomMenuConfig(AppConfig):
    name = "custommenus"
