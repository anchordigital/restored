import wagtail_factories

from contact_forms.models import FormPage


class FormPageFactory(wagtail_factories.PageFactory):
    intro = "Hello - this is a form to fill in!"
    thank_you_text = "Yay! Thanks for filling in my form!"

    class Meta:
        model = FormPage
