from django.test import TestCase

from .factories import FormPageFactory


class TestFormPageModel(TestCase):
    def test_init(self):
        page = FormPageFactory.create()
        self.assertIsNotNone(page.pk)
