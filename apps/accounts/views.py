from django.shortcuts import redirect
from django.template.response import TemplateResponse
from django.urls import reverse_lazy
from django.views.generic import CreateView, TemplateView

from .forms import UserRegistrationForm
from .models import User
from .utils import send_activation_email


class UserRegistrationFormView(CreateView):
    template_name = "registration/registration_form.html"
    success_url = reverse_lazy("registration_complete")
    form_class = UserRegistrationForm

    def form_valid(self, form):
        redirect = super(UserRegistrationFormView, self).form_valid(form)
        send_activation_email(self.request, self.object)
        return redirect


class RegistrationCompleteView(TemplateView):
    template_name = "registration/registration_complete.html"


def activate(request, pk, activation_key):
    try:
        user = User.objects.get(id=pk)

        # Activate the account if the key matches
        if user.activation_key and user.activation_key == activation_key:
            user.is_active = True
            user.save()
            user.activation_key = ""
            user.save()
            return redirect("registration_activate_complete")
    except User.DoesNotExist:
        pass

    return TemplateResponse(request, "registration/activate_failed.html")


class ActivateCompleteView(TemplateView):
    template_name = "registration/activate_complete.html"
