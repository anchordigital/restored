from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import EmailMessage
from django.template.loader import render_to_string


def send_activation_email(request, user):
    email_body = render_to_string(
        "registration/activation_email.txt",
        {"site": get_current_site(request), "protocol": request.scheme, "new_user": user,},
    )
    email = EmailMessage(
        subject="Restored Relationships account activation", body=email_body, to=[user.email]
    )

    email.send(fail_silently=False)
