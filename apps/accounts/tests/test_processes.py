"""
    Test the sign up / activation / etc processes.
"""

from django.core import mail
from django.urls import reverse

from django_webtest import WebTest

from accounts.models import User


class TestRegistration(WebTest):
    def test_registration_happy_path(self):
        username = "testuser"
        email = "testuser@test.com"
        password = "this_is_4_Password?"

        page = self.app.get(reverse("registration_register"))
        form = page.forms["registration-form"]
        form["username"] = username
        form["password1"] = password
        form["password2"] = password
        form["terms_and_conditions"] = True
        form["email"] = email
        form["anti_spam"] = email[::-1]  # reverse, in l33t python.
        form.submit().follow()
        self.assertEqual(User.objects.count(), 1)
        user = User.objects.first()

        # Check email:
        self.assertEqual(len(mail.outbox), 1)
        activation_url = reverse(
            "registration_activate", kwargs={"pk": user.pk, "activation_key": user.activation_key}
        )
        self.assertIn("http://testserver" + activation_url, mail.outbox[0].body)

        # Follow link, activate account:
        self.app.get(activation_url)
        user.refresh_from_db()

        self.assertEqual(user.activation_key, "")
