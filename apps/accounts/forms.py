import string

from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.utils.crypto import get_random_string

from wagtail.users.forms import UserCreationForm as WagtailUserCreationForm, UserEditForm

from .models import User


class UserRegistrationForm(UserCreationForm):
    error_css_class = "error"
    required_css_class = "required"

    def __init__(self, *args, **kwargs):
        super(UserCreationForm, self).__init__(*args, **kwargs)
        help = "For better security, try choosing four random words that are hard to guess."
        self.fields["password1"].help_text = help

    class Meta(UserCreationForm.Meta):
        model = User
        fields = (
            "username",
            "password1",
            "password2",
            "email",
            "first_name",
            "last_name",
            "location",
            "terms_and_conditions",
        )
        help_texts = {
            "last_name": (
                "Don't worry, your first and last names will never be seen by other users."
            ),
        }

    username = forms.RegexField(
        label="Pick a screen-name",
        max_length=30,
        regex=r"^[\w.@+-]+$",
        help_text=("Please ensure that you cannot be identified from the screen name."),
        error_messages={
            "invalid": ("This value may contain only letters, numbers and @/./+/-/_ characters.")
        },
    )
    email = forms.EmailField(
        label="Email address", help_text="Your email address will never be seen by other users."
    )
    location = forms.CharField(max_length=100, required=False)
    terms_and_conditions = forms.BooleanField(
        help_text=(
            'I agree to the <a href="/network-terms/" target="_blank">Terms &amp; Conditions</a> '
            "of the site"
        )
    )
    website = forms.URLField(required=False, widget=forms.HiddenInput())
    anti_spam = forms.CharField(required=False, widget=forms.HiddenInput())

    def clean_website(self):
        website = self.cleaned_data["website"]

        if website:
            raise forms.ValidationError("This field should be left blank")

        return website

    def clean(self):
        cleaned_data = super().clean()
        email = cleaned_data.get("email")
        anti_spam = cleaned_data.get("anti_spam")

        if email:
            if anti_spam != email[::-1]:
                raise forms.ValidationError(
                    "Anti spam error - javascript is required for registration"
                )

        return cleaned_data

    def save(self, commit=True):
        user = super().save(commit=False)
        user.is_active = False
        user.set_password(self.cleaned_data["password1"])
        user.location = (self.cleaned_data["location"],)
        user.activation_key = get_random_string(length=12, allowed_chars=string.hexdigits[:16])

        if commit:
            user.save()

        return user


class CustomUserEditForm(UserEditForm):
    location = forms.CharField(required=False)
    activation_key = forms.CharField(required=False)


class CustomUserCreateForm(WagtailUserCreationForm):
    location = forms.CharField(required=False)
    activation_key = forms.CharField(required=False)
