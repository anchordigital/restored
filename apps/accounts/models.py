from django.contrib.auth.models import AbstractUser
from django.db import models

from .managers import UserManager


class User(AbstractUser):
    location = models.CharField(max_length=100, blank=True)
    activation_key = models.CharField(max_length=12, blank=True, db_index=True)

    manager = UserManager()

    def __str__(self):
        return (
            f"{self.first_name + ' '}{self.last_name}".strip()
            or f"({self.email if self.email else self.username or 'unknown'})"
        )
