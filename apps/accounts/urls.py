from django.contrib.auth.urls import urlpatterns as auth_urlpatterns
from django.urls import path

from . import views

urlpatterns = [
    path("register/", views.UserRegistrationFormView.as_view(), name="registration_register"),
    path(
        "register/complete/",
        views.RegistrationCompleteView.as_view(),
        name="registration_complete",
    ),
    # Activation
    path("activate/<int:pk>/<str:activation_key>/", views.activate, name="registration_activate",),
    path(
        "activate/complete/",
        views.ActivateCompleteView.as_view(),
        name="registration_activate_complete",
    ),
]

urlpatterns += auth_urlpatterns
