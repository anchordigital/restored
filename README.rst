=====================
Good Bear notes
=====================


source env/bin/activate

Wagtail & Docker
=================
https://learnwagtail.com/tutorials/how-install-wagtail-docker/
https://selectfrom.dev/how-to-configure-django-postgresql-environment-using-docker-8d5bb2db447c


Importing DB
=================
Import the Databae after running docker build, but before running first migration


M1 Issues
=================
https://medium.com/geekculture/docker-build-with-mac-m1-d668c802ab96


NGINX
=================
https://www.devopsschool.com/blog/configure-nginx-in-host-server-with-https-enabled-and-route-all-traffic-container/
https://learnwagtail.com/launch-your-wagtail-website-digital-ocean-ubuntu-18/


SSL & LETSENCRYPT
================
https://www.digitalocean.com/community/tutorials/how-to-secure-nginx-with-let-s-encrypt-on-ubuntu-20-04


FABFILE & DEPLOY
================
https://hackernoon.com/deployment-automation-via-ssh-with-python-fabric-how-it-works-w0q33wn

FIREWALL
================
https://www.digitalocean.com/community/tutorials/how-to-set-up-a-firewall-with-ufw-on-ubuntu-20-04








=====================
restoredrelationships
=====================

Development setup
=================

It's recommended you use `virtualenvwrapper <https://virtualenvwrapper.readthedocs.io/en/latest/>`_
and `The Developer Society Dev Tools <https://github.com/developersociety/tools>`_.

Presuming you are using those tools, getting started on this project is pretty straightforward:

.. code:: console

    $ dev-clone restoredrelationships
    $ workon restoredrelationships
    $ make reset

You can now run the development server:

.. code:: console

    $ python manage.py runserver


Important to know about this project
====================================

There is a custom Document model (resources.models.File) which is used to allow purchasable
products via their online store.

The online store is with Square / Weebly - and we receive webhooks when orders are updated,
and send unique download codes for files by email to customers.

Testing Orders with Square
==========================

It's a bit of a pain, TBH.  As it always is, with payment stuff.

Make sure you have the sandbox environment variables.

.. code:: console

    export SQUARE_APPLICATION_ID=''
    export SQUARE_ACCESS_TOKEN=''
    export SQUARE_ENVIRONMENT='sandbox'

or whatever sandbox settings you need. You can create new ones on the square developer site,
https://developer.squareup.com/apps/.

You'll need to log into the dashboard and set up a webhook, we're only interested in the
``order.updated`` one.

So set up ngrok or whatever proxy to your local installation, and then set the webhook to point
at ``https://<YOUR_NGROK_URL>/get/webhook/``.  Only https URLS will work, then get the signature
key of the webhook from Square, and set it in the wagtail admin site settings.

Create a document upload with the SKU of ``TEST1_EN``.

The real annoying things is that square doesn't (as of June 2020) have a way to create
normal sandbox checkout links like the real store does - you can only create single-use
ones.  So there's a script:

Run the script ``python3 ./apps/resources/scripts/create_sandbox_products.py``
which deletes all old products in the sandbox, creates a new one, and then creates a one-time
download link for you.  Feel free to copy the script and edit it to make better versions
for testing for you...

You can use that new URL as a purchase URL in the Resource Page, and check your console log
to see the requests and emails going through - or just open the URL directly if you're just
testing the webhook.

Use the test card numbers from
https://developer.squareup.com/docs/testing/test-values.
